#include "Power4.h"

const int row_count = 6;
const int column = 7;

int **boards(int **matrice)
{
    // Init matrice :

    matrice = (int **)malloc(row_count * sizeof(int *));
    for (int i = 0; i < row_count; i++)
    {
        matrice[i] = (int *)malloc(column * sizeof(int));
        for (int j = 0; j < column; j++)
        {
            matrice[i][j] = 0;
        }
    }

    return matrice;
}

int get_next_open_row(int **board, int colone)
{
    int row = row_count - 1;

    while ((board[row][colone] == 1 || board[row][colone] == 2) && row != -1)
    {
        row = row - 1;
        if (row == -1)
        {
            return -1;
        }
    }
    return row;
}

bool is_valid_location(int **board, int *r, int colone)
{
    if (column > colone && colone >= 0)
    {
        int row = get_next_open_row(board, colone);

        if (row != -1 && board[row][colone] == 0)
        {
            *r = row;
            printf("row : %d\n", row);
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

void drop_piece(int **board, int colone, int player)
{
    board[get_next_open_row(board, colone)][colone] = player;
}

bool check_diagonal(int **board)
{
    // Vérification des alignements en diagonale vers le haut
    for (int row = row_count - 1; row >= 3; row--)
    {
        for (int col = 0; col <= column - 4; col++)
        {
            if (board[row][col] != 0 &&
                board[row][col] == board[row - 1][col + 1] &&
                board[row][col] == board[row - 2][col + 2] &&
                board[row][col] == board[row - 3][col + 3])
            {
                return true;
            }
        }
    }

    // Vérification des alignements en diagonale vers le bas
    for (int row = 0; row <= row_count - 4; row++)
    {
        for (int col = 0; col <= column - 4; col++)
        {
            if (board[row][col] != 0 &&
                board[row][col] == board[row + 1][col + 1] &&
                board[row][col] == board[row + 2][col + 2] &&
                board[row][col] == board[row + 3][col + 3])
            {
                return true;
            }
        }
    }

    return false;
}

bool check_column(int **board)
{
    for (int row = 0; row <= row_count - 4; row++)
    {
        for (int col = 0; col < column; col++)
        {
            if (board[row][col] != 0 &&
                board[row][col] == board[row + 1][col] &&
                board[row][col] == board[row + 2][col] &&
                board[row][col] == board[row + 3][col])
            {
                return true;
            }
        }
    }

    return false;
}

bool check_line (int ** board)
{
     for (int col = 0; col <= column - 4; col++)
    {
        for (int row = 0; row <  row_count; row++)
        {
            if (board[row][col] != 0 &&
                board[row][col] == board[row ][col+1] &&
                board[row][col] == board[row ][col+2] &&
                board[row][col] == board[row ][col+3])
            {
                return true;
            }
        }
    }

    return false;
}






bool game_over(int **board)
{
    if (check_diagonal(board) || check_column(board) || check_line(board))
    {
        return true;
    }
    else
    {
        return false;
    }
}

void affichage(int **board)
{
    for (int i = 0; i < row_count; i++)
    {
        printf("\n");
        for (int j = 0; j < column; j++)
        {
            printf(" %d ", board[i][j]);
        }
    }
}

void game(int **board, SDL_Window *window, SDL_Renderer *renderer)
{

    board = boards(board);
    bool game_end = false;
    int turn = 0;

    SDL_Rect
        window_dimensions = {0};
    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h); // Dimensions de la fenêtre
    // Tracer le rectangle bleu
    tracerRect(window_dimensions, renderer);

    while (!game_end && turn != 42)
    {
        affichage(board);

        // player1
        if (turn % 2 == 0)
        {
            int row;

            SDL_bool
                program_on = SDL_TRUE,   // Booléen pour dire que le programme doit continuer
                event_utile = SDL_FALSE; // Booléen pour savoir si on a trouvé un event traité
            SDL_Event event;             // Evènement à traiter

            while (program_on && turn % 2 == 0) // La boucle des évènements
            {
                event_utile = SDL_FALSE;
                while (!event_utile && SDL_PollEvent(&event) && turn % 2 == 0)
                { 
                    switch (event.type)
                    {              // En fonction de la valeur du type de cet évènement
                    case SDL_QUIT: // Si X de la fenêtre : on quitte le programme
                        program_on = SDL_FALSE;
                        event_utile = SDL_TRUE;
                        break;
                    case SDL_KEYDOWN:           // Si touche appuyée
                    switch (event.key.keysym.sym) {
                        case SDLK_ESCAPE:       // Si échap : quitte la fenêtre
                            SDL_Quit();
                            break;
                    }
                    case SDL_MOUSEBUTTONDOWN: // Si touche appuyée
                        int x;
                        int y;
                        SDL_GetMouseState(&x, &y);
                        if ((x > 20 && x < (window_dimensions.w+170)/7) && SDL_BUTTON(SDL_BUTTON_RIGHT)) // Première case
                        {
                            if (is_valid_location(board, &row, 0))
                            {
                                drop_piece(board, 0, 1);
                                dessinerPion(0, get_next_open_row(board, 0) + 2, renderer, window_dimensions, turn);
                                turn += 1;
                                game_end = game_over(board);
                                printf("le bool %d\n", game_end);
                                if (game_end)
                                {
                                    printf("Player 1 win");
                                }
                            }
                        }
                        else if ((x > (window_dimensions.w+170)/7 && x < 2*(window_dimensions.w+170)/7) && SDL_BUTTON(SDL_BUTTON_RIGHT)) // Deuxième case
                        {
                            if (is_valid_location(board, &row, 0))
                            {
                                drop_piece(board, 1, 1);
                                dessinerPion(1, get_next_open_row(board, 1) + 2, renderer, window_dimensions, turn);
                                turn += 1;
                                game_end = game_over(board);
                                printf("le bool %d\n", game_end);
                                if (game_end)
                                {
                                    printf("Player 1 win");
                                }
                            }
                        }
                        else if ((x > 2*(window_dimensions.w+170)/7 && x < 3*(window_dimensions.w+170)/7) && SDL_BUTTON(SDL_BUTTON_RIGHT)) // Deuxième case
                        {
                            if (is_valid_location(board, &row, 0))
                            {
                                drop_piece(board, 2, 1);
                                dessinerPion(2, get_next_open_row(board, 2) + 2, renderer, window_dimensions, turn);
                                turn += 1;
                                game_end = game_over(board);
                                printf("le bool %d\n", game_end);
                                if (game_end)
                                {
                                    printf("Player 1 win");
                                }
                            }
                        }
                        else if ((x > 3*(window_dimensions.w+170)/7 && x < 4*(window_dimensions.w+170)/7) && SDL_BUTTON(SDL_BUTTON_RIGHT)) // Deuxième case
                        {
                            if (is_valid_location(board, &row, 0))
                            {
                                drop_piece(board, 3, 1);
                                dessinerPion(3, get_next_open_row(board, 3) + 2, renderer, window_dimensions, turn);
                                turn += 1;
                                game_end = game_over(board);
                                printf("le bool %d\n", game_end);
                                if (game_end)
                                {
                                    printf("Player 1 win");
                                }
                            }
                        }
                        else if ((x > 4*(window_dimensions.w+170)/7 && x < 5*(window_dimensions.w+170)/7) && SDL_BUTTON(SDL_BUTTON_RIGHT)) // Deuxième case
                        {
                            if (is_valid_location(board, &row, 0))
                            {
                                drop_piece(board, 4, 1);
                                dessinerPion(4, get_next_open_row(board, 4) + 2, renderer, window_dimensions, turn);
                                turn += 1;
                                game_end = game_over(board);
                                printf("le bool %d\n", game_end);
                                if (game_end)
                                {
                                    printf("Player 1 win");
                                }
                            }
                        }
                        else if ((x > 5*(window_dimensions.w+170)/7 && x < 6*(window_dimensions.w+170)/7) && SDL_BUTTON(SDL_BUTTON_RIGHT)) // Deuxième case
                        {
                            if (is_valid_location(board, &row, 0))
                            {
                                drop_piece(board, 5, 1);
                                dessinerPion(5, get_next_open_row(board, 5) + 2, renderer, window_dimensions, turn);
                                turn += 1;
                                game_end = game_over(board);
                                printf("le bool %d\n", game_end);
                                if (game_end)
                                {
                                    printf("Player 1 win");
                                }
                            }
                        }
                        else if ((x > 6*(window_dimensions.w+170)/7 && x < 7*(window_dimensions.w+170)/7) && SDL_BUTTON(SDL_BUTTON_RIGHT)) // Deuxième case
                        {
                            if (is_valid_location(board, &row, 0))
                            {
                                drop_piece(board, 6, 1);
                                dessinerPion(6, get_next_open_row(board, 6) + 2, renderer, window_dimensions, turn);
                                turn += 1;
                                game_end = game_over(board);
                                printf("le bool %d\n", game_end);
                                if (game_end)
                                {
                                    printf("Player 1 win");
                                }
                            }
                        }
                        break;
                    }
                }
            }

            printf("sortie de la boucle\n");
        }

    // player2
    else
    {
        int row;

            SDL_bool
                program_on = SDL_TRUE,   // Booléen pour dire que le programme doit continuer
                event_utile = SDL_FALSE; // Booléen pour savoir si on a trouvé un event traité
            SDL_Event event;             // Evènement à traiter

            while (program_on && turn % 2 != 0) // La boucle des évènements
            {
                event_utile = SDL_FALSE;
                while (!event_utile && SDL_PollEvent(&event) && turn % 2 != 0)
                {
                    switch (event.type)
                    {              // En fonction de la valeur du type de cet évènement
                    case SDL_QUIT: // Si X de la fenêtre : on quitte le programme
                        program_on = SDL_FALSE;
                        event_utile = SDL_TRUE;
                        break;
                    case SDL_MOUSEBUTTONDOWN: // Si touche appuyée
                        int x;
                        int y;
                        SDL_GetMouseState(&x, &y);
                        if ((x > 20 && x < (window_dimensions.w + 170) / 7) && SDL_BUTTON(SDL_BUTTON_RIGHT)) // Première case
                        {
                            if (is_valid_location(board, &row, 0))
                            {
                                drop_piece(board, 0, 2);
                                dessinerPion(0, get_next_open_row(board, 0) + 2, renderer, window_dimensions, turn);
                                turn += 1;
                                game_end = game_over(board);
                                if (game_end)
                                {
                                    printf("Player 2 win");
                                }
                            }
                        }
                        else if ((x > (window_dimensions.w + 170) / 7 && x < 2 * (window_dimensions.w + 170) / 7) && SDL_BUTTON(SDL_BUTTON_RIGHT)) // Deuxième case
                        {
                            if (is_valid_location(board, &row, 0))
                            {
                                drop_piece(board, 1, 2);
                                dessinerPion(1, get_next_open_row(board, 1) + 2, renderer, window_dimensions, turn);
                                turn += 1;
                                game_end = game_over(board);
                                if (game_end)
                                {
                                    printf("Player 2 win");
                                }
                            }
                        }
                        else if ((x > 2 * (window_dimensions.w + 170) / 7 && x < 3 * (window_dimensions.w + 170) / 7) && SDL_BUTTON(SDL_BUTTON_RIGHT)) // Deuxième case
                        {
                            if (is_valid_location(board, &row, 0))
                            {
                                drop_piece(board, 2, 2);
                                dessinerPion(2, get_next_open_row(board, 2) + 2, renderer, window_dimensions, turn);
                                turn += 1;
                                game_end = game_over(board);
                                if (game_end)
                                {
                                    printf("Player 2 win");
                                }
                            }
                        }
                        else if ((x > 3 * (window_dimensions.w + 170) / 7 && x < 4 * (window_dimensions.w + 170) / 7) && SDL_BUTTON(SDL_BUTTON_RIGHT)) // Deuxième case
                        {
                            if (is_valid_location(board, &row, 0))
                            {
                                drop_piece(board, 3, 2);
                                dessinerPion(3,get_next_open_row(board, 3) + 2, renderer, window_dimensions, turn);
                                turn += 1;
                                game_end = game_over(board);
                                if (game_end)
                                {
                                    printf("Player 2 win");
                                }
                            }
                        }
                        else if ((x > 4 * (window_dimensions.w + 170) / 7 && x < 5 * (window_dimensions.w + 170) / 7) && SDL_BUTTON(SDL_BUTTON_RIGHT)) // Deuxième case
                        {
                            if (is_valid_location(board, &row, 0))
                            {
                                drop_piece(board, 4, 2);
                                dessinerPion(4, get_next_open_row(board, 4) + 2, renderer, window_dimensions, turn);
                                turn += 1;
                                game_end = game_over(board);
                                printf("le bool %d\n", game_end);
                                if (game_end)
                                {
                                    printf("Player 2 win");
                                }
                            }
                        }
                        else if ((x > 5 * (window_dimensions.w + 170) / 7 && x < 6 * (window_dimensions.w + 170) / 7) && SDL_BUTTON(SDL_BUTTON_RIGHT)) // Deuxième case
                        {
                            if (is_valid_location(board, &row, 0))
                            {
                                drop_piece(board, 5, 2);
                                dessinerPion(5,get_next_open_row(board, 5) + 2, renderer, window_dimensions, turn);
                                turn += 1;
                                game_end = game_over(board);
                                printf("le bool %d\n", game_end);
                                if (game_end)
                                {
                                    printf("Player 2 win");
                                }
                            }
                        }
                        else if ((x > 6 * (window_dimensions.w + 170) / 7 && x < 7 * (window_dimensions.w + 170) / 7) && SDL_BUTTON(SDL_BUTTON_RIGHT)) // Deuxième case
                        {
                            if (is_valid_location(board, &row, 0))
                            {
                                drop_piece(board, 6, 2);
                                dessinerPion(6, get_next_open_row(board, 6) + 2, renderer, window_dimensions, turn);
                                turn += 1;
                                game_end = game_over(board);
                                printf("le bool %d\n", game_end);
                                if (game_end)
                                {
                                    printf("Player 2 win");
                                }
                            }
                        }
                        break;
                    }
                }
            }
    }

    if (turn == 42 && !game_end)
    {
        printf("No winner");
    }
}
}

void liberer(int **matrice)
{

    for (int i = 0; i < row_count; i++)
    {
        free(matrice[i]); // Libérer chaque ligne de la matrice
    }

    free(matrice); // Libérer le tableau de pointeurs vers les lignes
    matrice = NULL;
}

void tracerRect(SDL_Rect window_dimensions, SDL_Renderer *renderer)
{
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);

    SDL_Rect rectangle_bleu = {0}; // Rectangle à remplir de bleu
    rectangle_bleu.x = 20;
    rectangle_bleu.y = 150;

    // Rectangle bleu :
    rectangle_bleu.w = window_dimensions.w - 40;
    rectangle_bleu.h = window_dimensions.h - 150;
    SDL_SetRenderDrawColor(renderer, 0, 128, 255, 255); // Couleur bleu
    SDL_RenderFillRect(renderer, &rectangle_bleu);
    SDL_RenderPresent(renderer);

    // Ronds :
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    for (int i = 0; i < 7; i++)
    {
        for (int j = 0; j < 7; j++)
        {
            // Un rond :
            for (float angle = 0; angle < 2 * 3.1444444; angle += 3.1444444 / 400)
            {
                SDL_RenderDrawPoint(renderer, i * window_dimensions.w / 9 + 130 + window_dimensions.w / 19 * cos(angle), j * window_dimensions.w / 9 + 200 + window_dimensions.w / 19 * sin(angle));

                for (int k = 0; k < 500; k++) // On remplit le cercle
                {
                    SDL_RenderDrawPoint(renderer, i * window_dimensions.w / 9 + 130 + window_dimensions.w / (19 + k) * cos(angle), j * window_dimensions.w / 9 + 200 + window_dimensions.w / (19 + k) * sin(angle));
                }
            }
        }
    }
    SDL_RenderPresent(renderer);
}

void dessinerPion(int ligne, int colonne, SDL_Renderer *renderer, SDL_Rect window_dimensions, int turn)
{
    if (turn % 2 == 0)
    {
        SDL_SetRenderDrawColor(renderer, 240, 0, 32, 255);
    }
    else if (turn % 2 != 0)
    {
        SDL_SetRenderDrawColor(renderer, 255, 255, 0, 255);
    }
    // Un rond :
    for (float angle = 0; angle < 2 * 3.1444444; angle += 3.1444444 / 400)
    {
        SDL_RenderDrawPoint(renderer, ligne * window_dimensions.w / 9 + 130 + window_dimensions.w / 19 * cos(angle), colonne * window_dimensions.w / 9 + 200 + window_dimensions.w / 19 * sin(angle));

        for (int k = 0; k < 500; k++) // On remplit le cercle
        {
            SDL_RenderDrawPoint(renderer, ligne * window_dimensions.w / 9 + 130 + window_dimensions.w / (19 + k) * cos(angle), colonne * window_dimensions.w / 9 + 200 + window_dimensions.w / (19 + k) * sin(angle));
        }
    }
    SDL_RenderPresent(renderer);
}