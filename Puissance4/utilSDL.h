#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>

// Permet d'initialiser une fenêtre
SDL_Window * initWindow(int x, int y, int w, int h, char * name);

// Permet d'initialiser un Renderer
SDL_Renderer * initRenderer(SDL_Window * window);

// Arrêter tout le rendu en SDL, en vérifiant ce qui ne va pas
void end_sdl(char ok,  char const *msg, SDL_Window *window, SDL_Renderer *renderer);

// Permet d'initialiser un texte en passant en paramètre la fenêtre, le renderer, le texte et la position où le mettre
SDL_Texture * initText(SDL_Window * window, SDL_Renderer * renderer, char * text, SDL_Rect pos);