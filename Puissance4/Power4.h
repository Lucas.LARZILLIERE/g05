#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include "utilSDL.h"

int  ** boards(int ** matrice);
int get_next_open_row(int ** board, int colone);
bool is_valid_location(int **board, int * row, int colone);
void drop_piece(int ** board,int colone,int player);
bool check_diagonal(int ** board);
bool check_column(int ** board);
bool game_over(int ** board);
void game(int **board, SDL_Window *w, SDL_Renderer * r);
void liberer(int** matrice);
void tracerRect(SDL_Rect window_dimensions, SDL_Renderer * renderer);
void dessinerPion(int ligne, int colonne, SDL_Renderer * renderer, SDL_Rect window_dimensions, int turn);
bool check_line (int ** board);