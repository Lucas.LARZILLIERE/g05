#include <stdio.h>
#include "paveSerpent_SDL.h"

void draw(SDL_Renderer *renderer)
{
    SDL_Rect rectangle;

    SDL_SetRenderDrawColor(renderer,
                           255, 255, 255, // mode Red, Green, Blue (tous dans 0..255)
                           255);     // 0 = transparent ; 255 = opaque
    rectangle.x = 0;                 // x haut gauche du rectangle
    rectangle.y = 0;                 // y haut gauche du rectangle
    rectangle.w = 400;               // sa largeur (w = width)
    rectangle.h = 400;               // sa hauteur (h = height)

    SDL_RenderFillRect(renderer, &rectangle);

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderDrawLine(renderer,
                       10, 10,      // x,y du point de la première extrémité
                       390, 10); // x,y seconde extrémité
    SDL_RenderDrawLine(renderer,
                       390, 10,      // x,y du point de la première extrémité
                       390, 390); // x,y seconde extrémité
    SDL_RenderDrawLine(renderer,
                       390, 390,      // x,y du point de la première extrémité
                       10, 390); // x,y seconde extrémité
    SDL_RenderDrawLine(renderer,
                       10, 390,      // x,y du point de la première extrémité
                       10, 10); // x,y seconde extrémité

    int a = 10;
    int b = 10;
    int c = 390;
    int d = 390;
    for (int i = 0; i <500; i++)
    {
        SDL_RenderDrawLine(renderer,
                        a, b,      // x,y du point de la première extrémité
                        c, a); // x,y seconde extrémité
        SDL_RenderDrawLine(renderer,
                        c, a,      // x,y du point de la première extrémité
                        d, c); // x,y seconde extrémité
        SDL_RenderDrawLine(renderer,
                        d, c,      // x,y du point de la première extrémité
                        b, d); // x,y seconde extrémité
        SDL_RenderDrawLine(renderer,
                        b, d,      // x,y du point de la première extrémité
                        a, b); // x,y seconde extrémité

        a += 2;
        b += 1;
        c -= 1;
        d -= 2;
        SDL_SetRenderDrawColor(renderer, 10*a, 10*b, 10*c, 10*d);
        SDL_RenderPresent(renderer);
        SDL_Delay(10);
    }


    
}

int paveSerpent()
{
    SDL_Window* window = NULL;
       SDL_Renderer* renderer = NULL;

       SDL_DisplayMode screen;

       SDL_GetCurrentDisplayMode(0, &screen);

       /* Création de la fenêtre */
       window = SDL_CreateWindow("Premier dessin",
                 SDL_WINDOWPOS_CENTERED,
                 SDL_WINDOWPOS_CENTERED,
                 400,
                 400,
                 SDL_WINDOW_OPENGL);
       if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

       /* Création du renderer */
       renderer = SDL_CreateRenderer(window, -1,
                     SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
       if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

       draw(renderer); 
       SDL_RenderPresent(renderer);
       SDL_Delay(10000);
                                                       

       /* on referme proprement la SDL */
       end_sdl(1, "Normal ending", window, renderer);
       return EXIT_SUCCESS;
}