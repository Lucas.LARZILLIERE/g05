#include "x_SDL.h"
#include <SDL2/SDL_image.h>

SDL_Texture *load_texture_from_image(char *file_image_name, SDL_Window *window, SDL_Renderer *renderer);
void animateSprite(SDL_Texture *my_texture,
                   SDL_Window *window,
                   SDL_Renderer *renderer);