#include <stdio.h>
#include "x_SDL.h"

void moveWindow(SDL_Window * window)
{
    SDL_bool
        program_on = SDL_TRUE,   // Booléen pour dire que le programme doit continuer
        paused = SDL_FALSE,      // Booléen pour dire que le programme est en pause
        event_utile = SDL_FALSE; // Booléen pour savoir si on a trouvé un event traité
    SDL_Event event;             // Evènement à traiter


    // On récupère la taille de l'écran :
    SDL_DisplayMode mode;
    SDL_GetCurrentDisplayMode(0, &mode);

    while (program_on) // La boucle des évènements
    {
        
        // On va stocker la position de la fenêtre à chaque tour de boucle pour la modifier plus tard
        int curX;
        int curY;
        int curW;
        int curH;
        SDL_GetWindowPosition(window, &curX, &curY);
        SDL_GetWindowSize(window, &curW, &curH);

        event_utile = SDL_FALSE;
        while (!event_utile && SDL_PollEvent(&event))
        {
            switch (event.type){ // En fonction de la valeur du type de cet évènement                         
            case SDL_QUIT:              // Si X de la fenêtre : on quitte le programme
                program_on = SDL_FALSE; 
                event_utile = SDL_TRUE;
                break;
            case SDL_KEYDOWN:           // Si touche appuyée
            switch (event.key.keysym.sym) {
                case SDLK_UP:           // Si touche du haut, déplace la fenêtre vers le haut
                    SDL_SetWindowPosition(window, curX, curY-10);
                    event_utile = SDL_TRUE;
                    break;
                case SDLK_DOWN:         // Si touche du bas, déplace la fenêtre vers le bas
                    SDL_SetWindowPosition(window, curX, curY+10);
                    event_utile = SDL_TRUE;
                    break;
                case SDLK_RIGHT:        // Si touche de droite, déplace la fenêtre vers la droite
                    SDL_SetWindowPosition(window, curX+10, curY);
                    SDL_SetWindowSize(window, curW+10, curH+10);
                    event_utile = SDL_TRUE;
                    break;
                case SDLK_LEFT:         // Si touche de gauche, déplace la fenêtre vers la gauche
                    SDL_SetWindowPosition(window, curX-10, curY);
                    SDL_SetWindowSize(window, curW-10, curH-10);
                    event_utile = SDL_TRUE;
                    break;
                case SDLK_ESCAPE:       // Si échap : quitte la fenêtre
                    SDL_Quit();
                }
            }
        }
    }

    SDL_Quit();
}