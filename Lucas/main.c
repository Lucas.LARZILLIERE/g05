#include <stdio.h>
#include "paveSerpent_SDL.h"
#include "x_SDL.h"
#include "textures_SDL.h"
#include "utilSDL.h"

int main()
{   
    /* Initialisation de la SDL  + gestion de l'échec possible */
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        SDL_Log("Error : SDL initialisation - %s\n",
                SDL_GetError()); // l'initialisation de la SDL a échoué
        exit(EXIT_FAILURE);
    }

    char numero;
    printf("Quel exercice voulez-vous ?\n1 : exercice sur les fenêtres\n2 : exercice sur les formes géométriques\n3 : exercice sur les sprites\n");
    scanf("%c", &numero);

    if (numero == '1')
    {
        // Initialisation de la fenêtre et du renderer
        SDL_Window * w = NULL;
        w = initWindow(100, 100, 500, 500, "Fenêtre");
        moveWindow(w);
    }
    else if (numero == '2')
    {
        paveSerpent();
    }
    else if (numero == '3')
    {
        // Initialisation de la fenêtre et du renderer
        SDL_Window * w = NULL;
        SDL_Renderer * r = NULL;
        w = initWindow(100, 100, 500, 500, "Fenêtre");
        r  = initRenderer(w);

        SDL_Texture * t = NULL;
        t = load_texture_from_image("cute_apple_run.png", w, r);
        animateSprite(t, w, r);
    }
}