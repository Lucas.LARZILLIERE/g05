#include "textures_SDL.h"

SDL_Texture *load_texture_from_image(char *file_image_name, SDL_Window *window, SDL_Renderer *renderer)
{
    SDL_Surface *my_image = NULL;   // Variable de passage
    SDL_Texture *my_texture = NULL; // La texture

    my_image = IMG_Load(file_image_name); // Chargement de l'image dans la surface
                                          // image=SDL_LoadBMP(file_image_name); fonction standard de la SDL,
                                          // uniquement possible si l'image est au format bmp */
    if (my_image == NULL)
        end_sdl(0, "Chargement de l'image impossible", window, renderer);

    my_texture = SDL_CreateTextureFromSurface(renderer, my_image); // Chargement de l'image de la surface vers la texture
    SDL_FreeSurface(my_image);                                     // la SDL_Surface ne sert que comme élément transitoire
    if (my_texture == NULL)
        end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer);

    return my_texture;
}

void animateSprite(SDL_Texture *my_texture, SDL_Window *window, SDL_Renderer *renderer)
{
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);

    ///////////////////////
    // Init personnage ////
    ///////////////////////
    SDL_Rect
        source = {0},            // Rectangle définissant la zone totale de la planche
        window_dimensions = {0}, // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
        destination = {0},       // Rectangle définissant où la zone_source doit être déposée dans le renderer
        state = {0};             // Rectangle de la vignette en cours dans la planche
    SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h);          // Dimensions de l'image
    int nb_images = 2;                   // Seulement 2 vignettes
    float zoom = 4;                      
    int offset_x = source.w / nb_images, 
        offset_y = source.h;             
    state.x = 0;
    state.y = 1;
    state.w = offset_x;
    state.h = offset_y;
    destination.w = offset_x * zoom;
    destination.h = offset_y * zoom;
    
    ///////////////////////
    ////// Init Epee //////
    ///////////////////////
    SDL_Rect
        source2 = {0},         
        destination2 = {0},    
        state2 = {0};             
    SDL_Texture * sword = load_texture_from_image("sword.png", window, renderer);
    SDL_QueryTexture(sword, NULL, NULL, &source2.w, &source2.h);     
    float zoom2 = 2;               
    int offset_x2 = source2.w,
        offset_y2 = source2.h;         
    state2.x = 0;      
    state2.y = 1;   
    state2.w = offset_x2;
    state2.h = offset_y2;
    destination2.w = offset_x2 * zoom2;
    destination2.h = offset_y2 * zoom2; 

    ///////////////////////
    /// Init Background ///
    ///////////////////////
    SDL_Rect 
        sourceBG = {0},                        
        destinationBG = {0};             
    SDL_Texture * bg = load_texture_from_image("bg.png", window, renderer);
    SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h);   

    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h); 

    destinationBG = window_dimensions;
    SDL_RenderCopy(renderer, bg, NULL, &destinationBG);     
    SDL_RenderPresent(renderer);

    destination.y = (window_dimensions.h - destination.h); // On positionne le personnage en bas de l'écran
    destination2.y = (window_dimensions.h - destination2.h); // On positionne l'épée en bas de l'écran
    

    ///////////////////////
    // Bouger personnage //
    ///////////////////////
    int xPos = 0;
    int leftRight = 0; // 0 si tourné vers la droite
     SDL_bool
        program_on = SDL_TRUE,   // Booléen pour dire que le programme doit continuer
        event_utile = SDL_FALSE; // Booléen pour savoir si on a trouvé un event traité
    SDL_Event event;             // Evènement à traiter

    while (program_on) // La boucle des évènements
    {
        xPos = destination.x;

        event_utile = SDL_FALSE;
        while (!event_utile && SDL_PollEvent(&event))
        {
            switch (event.type){ // En fonction de la valeur du type de cet évènement                         
            case SDL_QUIT:              // Si X de la fenêtre : on quitte le programme
                program_on = SDL_FALSE; 
                event_utile = SDL_TRUE;
                break;
            case SDL_KEYDOWN:           // Si touche appuyée
            switch (event.key.keysym.sym) {
                case SDLK_RIGHT:        // Si touche de droite, déplace le personnage vers la droite
                    destination.x = xPos + 5;  // Position en x pour l'affichage du sprite
                    state.x += offset_x; // On passe au prochain sprite
                    state.x %= source.w;
                    SDL_RenderClear(renderer);           // Effacer l'image précédente avant de dessiner la nouvelle
                    SDL_RenderCopy(renderer, bg, NULL, &destinationBG);
                    SDL_RenderCopy(renderer, my_texture, &state, &destination);
                    SDL_RenderPresent(renderer); // Affichage
                    event_utile = SDL_TRUE;
                    leftRight = 0;
                    break;
                case SDLK_LEFT:         // Si touche de gauche, déplace le personnage vers la gauche
                    destination.x = xPos - 5;  // Position en x pour l'affichage du sprite
                    state.x += offset_x; // On passe au prochain sprite
                    state.x %= source.w;
                    SDL_RenderClear(renderer);           // Effacer l'image précédente avant de dessiner la nouvelle
                    SDL_RenderCopy(renderer, bg, NULL, &destinationBG);
                    SDL_RenderCopyEx(renderer, my_texture, &state, &destination, 0, NULL, SDL_FLIP_HORIZONTAL);
                    SDL_RenderPresent(renderer); // Affichage
                    event_utile = SDL_TRUE;
                    leftRight = 1;
                    break;
                case SDLK_SPACE:         // Si touche espace, fait apparaitre une épée
                    if (leftRight == 0)
                    {
                        destination2.x = xPos + 100;  // Position en x pour l'affichage du sprite
                    }
                    else
                    {
                        destination2.x = xPos;  // Position en x pour l'affichage du sprite
                    }
                    state2.x += offset_x2; // On passe au prochain
                    state2.x %= source2.w; 
                    SDL_RenderCopy(renderer, sword, &state2, &destination2);
                    SDL_RenderPresent(renderer); // Affichage
                    event_utile = SDL_TRUE;
                    break;
                case SDLK_ESCAPE:       // Si échap : quitte la fenêtre
                    SDL_Quit();
                }
            }
        }
    }

    SDL_RenderClear(renderer); // Effacer la fenêtre avant de rendre la main
}