#include <stdio.h>
#include "creation_graphe.h"
#include "utilSDL.h"
#include <time.h>
#include "opti_exhau.h"
#include "methode_fourmies.h"
#include "exhaustif.h"
#include <stdbool.h>

int main()
{
    int taille = 10;
    srand(time(NULL));

    // Init matrice :
    int **Matrice;
    Matrice = (int **)malloc(taille * sizeof(int *));
    for (int i = 0; i < taille; i++)
    {
        Matrice[i] = (int *)malloc(taille * sizeof(int));
        for (int j = 0; j < taille; j++)
        {
            Matrice[i][j] = 0;
        }
    }

    // Génération matrice et affichage dans le terminal :
    genere(Matrice, 0, 9);
    genere_graphe(Matrice, 8, taille);

    // Création matrice après algo FW
    int **matriceFW;
    matriceFW = (int **)malloc(taille * sizeof(int *));
    for (int i = 0; i < taille; i++)
    {
        matriceFW[i] = (int *)malloc(taille * sizeof(int));
        for (int j = 0; j < taille; j++)
        {
            matriceFW[i][j] = 0;
        }
    }

    // Création matrice euclidienne
    int **matriceEuclid;
    matriceEuclid = (int **)malloc(taille * sizeof(int *));
    for (int i = 0; i < taille; i++)
    {
        matriceEuclid[i] = (int *)malloc(taille * sizeof(int));
        for (int j = 0; j < taille; j++)
        {
            matriceEuclid[i][j] = 0;
        }
    }
    // On initialise les coordonnées
    int **coord;
    coord = (int **)malloc(taille * sizeof(int *));
    for (int i = 0; i < taille; i++)
    {
        coord[i] = (int *)malloc(2 * sizeof(int));
        for (int j = 0; j < 2; j++)
        {
            coord[i][j] = 0;
        }
    }

    // Init window :
    SDL_Window *w = NULL;
    w = initWindow(0, 0, 800, 800, "Graphe");

    placerPoints(w, taille, Matrice, matriceEuclid, coord);

    afficherGrapheSDL(matriceEuclid, taille, coord, w); // On va récupérer la matrice euclidienne plus tard pour les recherches

    matriceFW = floydWarshall(matriceEuclid, taille);

    afficherMatrice(matriceFW, taille);
    afficherMatrice(matriceEuclid, taille);
    int L[10] = {1, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    int cp[1];
    cp[0] = 0;
    int odre[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    int distance;

    recherche_glouton(matriceFW, cp, &distance, L, 0, taille, odre);
    printf("Le chemin le plus court à une distance de %d étape\n", distance);
    printf("\n l'odre des points :");
    for (int i = 0; i < 10; i++)
    {
        printf(" %d ", odre[i]);
    }
    printf("\n \n");

    int *bestCycle = (int *)malloc((taille + 1) * sizeof(int));
    bestCycle = methodeFourmies(matriceFW, taille, 100, 200, 0.2);
    printf("La méthode de la fourmie montre que le meilleur chemin est :");
    for (int i = 0; i < taille; i++)
    {
        printf(" %d", bestCycle[i]);
    }
    printf("\nDe distance : ");
    printf(" %d \n", bestCycle[taille]);

    ///////////////
    // Exhaustif //
    ///////////////
    int * visites = (int*) malloc(taille*sizeof(int));
    int * permutationActuelle = (int*) malloc((taille+1)*sizeof(int));
    int * meilleurePermutation = (int*) malloc(taille*sizeof(int));
    int meilleurCout = 1000000;

    for (int i = 0; i < taille; i++)
    {
        visites[i] = 0;
        permutationActuelle[i] = 0;
        meilleurePermutation[i] = 0;
    }

    permutationActuelle[taille] = permutationActuelle[0];

    genererPermutations(0, taille, matriceFW, permutationActuelle, visites, &meilleurCout, meilleurePermutation);
    for (int i = 0; i < taille; i++)
    {
        printf("%d ", meilleurePermutation[i]);
    }
    printf("\nCoût minimal : %d\n", meilleurCout);
    free(visites);
    free(permutationActuelle);
    free(meilleurePermutation);

    TTF_Quit(); // Fermer le module de texte
}