#include "methode_fourmies.h"

float moy(int ** matrice,int taille) {
    int s=0,k=0,i,j;
    for (i=0; i<taille-1;i++) {
        for (j=i+1; j<taille;j++){
            if (matrice[i][j]!=0) {
                s+=matrice[i][j];
                k+=1;
            }
        }
    }
    return((float)s/k);
}

int * cycleFourmie(float ** pheromone,int ** matrice,int taille) {
    int* sommetVisites = (int*) malloc(taille * sizeof(int));       //def des variables
    for(int i=0;i<taille;i++)       //un sommet déjà visités sera à 0, donc on les initialise tous à 1,
        sommetVisites[i]=1;
    sommetVisites[0]=0;     //sauf le premier qu'on va traiter immédiatement;
    int* cycle = (int*) malloc((taille+1) * sizeof(int));
    float * interet = (float*) malloc(taille * sizeof(float)); 
    int etape=0, current=0, cumulatif, distance=0, randNumber,k;
    float totalProba;
    sommetVisites[0]=0;
    cycle[0]=0;
    
    while(etape<taille-1) {
        k=0;
        cumulatif=0;
        totalProba=0;
        for (int i=0;i<taille;i++)      //reset des intérêts
            interet[i]=0;
 
        for (int sommet=0; sommet<taille; sommet++) {       //calcul, pour chaque sommet restant, de l'intérêt de s'y rendre depuis le sommet current
            if (sommetVisites[sommet]!=0) {
                interet[k]=pow(pheromone[current][sommet], 1 ) / pow(matrice[current][sommet],1);
                k+=1;
            }
        }

        for (int i=0;i<k;i++)      //calcul de la somme des interet
            totalProba+=interet[i];

        for (int i=0;i<k;i++)      //transformation des intérêts en proba sur 1
            interet[i]=interet[i]/totalProba;   

        randNumber = rand()%101;  //nb aléatoire entre 1 et 100    
        k=0;
        cumulatif += (int)(round(interet[k]*100));

        while (randNumber >cumulatif && k<taille-etape-2) {        //chaque partition de l'espace [0;100] représente un sommet
                                                                //(car totalProba est la somme de la proba de chaque sommet),
                                                                //on cherche donc dans quelle partition l'nb aléatoire se trouve
            k+=1 ; 
            cumulatif += (int)(round(interet[k]*100));
        }
        
        for (int i=0;i<=k;i++){
            if(sommetVisites[i]==0)
                k+=1;
        }
        distance+= matrice[current][k];        //changement d'état vers le prochaine sommet
        sommetVisites[k]=0;
        etape+=1;
        cycle[etape]=k;
        current=k;
        
    }
    
    distance+= matrice[current][0];
    cycle[taille]=distance;        //longeur en poid du cycle à la fin de la liste
    /*printf("cycle=");                             //print le cycle de la fourmie
        for (int i=0;i<taille+1;i++)  {         
            printf("%d ",cycle[i]);
        }
        printf("\n");*/
    // Faut-il free ?
    free(sommetVisites);
    return cycle;
}



int * methodeFourmies(int ** matrice, int taille, int iterations,int nbeFourmies, float degradationsPhero) {
    int* bestCycle = (int*) malloc((taille+1) * sizeof(int));
    int* cycle = (int*) malloc((taille+1) * sizeof(int));
    float ** pheromone; 
    int bestDistance=0, distance;    

    float delta,q=moy(matrice,taille);            // q: moyenne des poids du graphe

    pheromone = (float**) malloc(taille *sizeof(float*));       //alloc et mise à 1 de la première matrice de pheromone
    for (int i = 0; i < taille; i++)                            //elle représente les pheromones à chaque vague
    {
        pheromone[i] = (float*) malloc(taille*sizeof(float));
        for (int j = 0; j < taille; j++)
            pheromone[i][j] = 1;
    }

    float ** newPheromone;                                        //alloc et mise à 0 de la seconde matrice de pheromone
    newPheromone = (float**) malloc(taille *sizeof(float*));       //elle sert à entreposer les nouvelles pheromones d'un vague avant
    for (int i = 0; i < taille; i++)                                //de les ajouter à la matrice pheromone une fois la vague finie
    {
        newPheromone[i] = (float*) malloc(taille*sizeof(float));
        for (int j = 0; j < taille; j++)
            newPheromone[i][j] = 0;
    }

    for (int vague=0; vague<iterations;vague++) {                   //boucle de 'iterations' vagues
       for (int fourmie=0; fourmie<nbeFourmies;fourmie++) {         //boucle de 'nbeFourmies' fourmies
            cycle=cycleFourmie(pheromone,matrice,taille);
            distance=cycle[taille];
            delta=q/distance;                   //coeff de pheromone à disperser

            if(bestDistance==0 || distance<bestDistance) {
                bestDistance=distance;
                memcpy(bestCycle,cycle,(taille+1)*sizeof(int));
            }

            for (int i=0;i<taille-1;i++) {                          //mise à jour de la matrice newPheromone
                newPheromone[cycle[i]][cycle[i+1]]+= delta;
                newPheromone[cycle[i+1]][cycle[i]]+= delta;
            }
            newPheromone[cycle[0]][cycle[taille-1]]+= delta; 
            newPheromone[cycle[taille-1]][cycle[0]]+= delta;     
        }

        for (int i=0;i<taille;i++){                     //mise à jour de pheromone et reset de newPheromone
            for (int j=0;j<taille;j++) {
                pheromone[i][j]= (degradationsPhero*newPheromone[i][j]+ (1-degradationsPhero)*pheromone[i][j]);
                newPheromone[i][j]=0;
            }
        }
    }
    for(int i=0;i<taille;i++){
        free(pheromone[i]);
        free(newPheromone[i]);
    }
    free(pheromone);
    free(newPheromone);
    return bestCycle;
}
/*
int main(){
    int taille =10;
    int ** Matrice;
    srand( time( NULL ) );
    Matrice = (int**) malloc(taille *sizeof(int*));
    for (int i = 0; i < taille; i++)
    {
        Matrice[i] = (int*) malloc(taille*sizeof(int));
        for (int j = 0; j < taille; j++)
        {
            Matrice[i][j] = 0;
        }
    }

    for(int i=0;i<taille;i++) {
        for (int j=i+1;j<taille;j++){
            Matrice[i][j]=(rand()%1200)+1;
            Matrice[j][i]=Matrice[i][j];
        }
    }

    for(int i=0;i<taille;i++){
        for(int j=0;j<taille;j++)
            printf("%d ",Matrice[i][j]);
        printf("\n");
    }
    int* bestCycle = (int*) malloc((taille+1) * sizeof(int));
    bestCycle=methodeFourmies(Matrice,taille,1000,100,0.1);
    printf("La méthode de la fourmie montre que le meilleur chemin est :");
    for (int i=0;i<taille+1;i++){
        printf(" %d",bestCycle[i]);
    }
    printf("\n De distance : ");
    printf(" %d",bestCycle[taille]);
    printf("\n");
    return 0;
} */     //gcc methode_fourmies.c -o methode_fourmies -Wall -Wextra -lm