#include <stdlib.h>
#include "utilSDL.h"
 

void genere(int ** Matrice, int bas, int haut);
void afficherMatrice(int ** matrice, int taille);
void genere_graphe(int ** Matrice, int p, int taille);
void afficherGrapheSDL(int ** matriceEuclid, int taille, int ** coords, SDL_Window * window);
void placerPoints(SDL_Window * window, int taille, int ** matrice, int ** matriceEuclid, int ** coords);