#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <math.h>       //besoin de -lm quand gcc
#define ALPHA 0.9       //Plus il est grand, plus les fourmies vont suivre les pheromones, default:0.9
#define BETA 1.5        //Plus il est grand, moins les fourmies vont explorer des cycles différents, default:1.5

float moy(int ** matrice,int taille);   //renvoie un float : moyenne des poids du graphe.
int * cycleFourmie(float ** pheromone,int ** matrice,int taille);      //Construction du cycle d'une fourmie.
                                                                        //Renvoie un pointeur de tableau de taille 'taille+1' avec le cycle 
                                                                        //et le poid du cycle en dernier élément.
int * methodeFourmies(int ** matrice, int taille, int iterations,int nbeFourmies, float degradationsPhero);    //Algo principal
                //Renvoie un pointeur de tableau de taille 'taille+1' ,avec le meilleur cycle 
                //et le poid de ce cycle en dernier élément, du graphe représenté par la matrice
                //carrée 'matrice' de taille 'taille'.
                //Il fera parcourir 'nbeFourmies' fourmies en 'iterations' vague avec un taux 
                //de disparation de pheromones par vague de 'degradationsPhero', default:0.1