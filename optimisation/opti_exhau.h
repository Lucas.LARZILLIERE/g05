#include <stdlib.h>

#include <stdio.h>
#include "utilSDL.h"

int recherche_meilleur_voisin(int ** Matrice , int * Listes_des_points , int  point_actuel, int taille);
int recherche_glouton(int ** Matrice, int * cp ,int* distance , int  * Liste_des_points , int  point_actuel, int taille, int * ordre);
int ** floydWarshall(int ** Matrice,int taille);