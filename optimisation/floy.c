int ** floydWarshall(int ** Matrice,int taille) {
    int ** distance = malloc(taille * sizeof(int));
    for(int i=0; i< taille;i++)
    {
        distance[i] = malloc(taille * sizeof(int));
    }
    for (int i = 0; i < taille; i++)
    {
        distance[i][i]=0;
        for (int j = 0; i < taille; j++)
        {
            if (Matrice[i][j]==1)
            {
                distance[i][j]=1;
                distance[j][i]=1;
            }
            
        }
        
    }
    
    
    for (int k = 0; k < taille; k++)
     {
        for (int i = 0; i < taille; i++) {
            for (int j = 0; j < taille; j++)
             {
                if (distance[k][j] + distance[i][k] < distance[i][j])
                {
                     distance[i][j] = distance[i][k] + distance[k][j];
                } 
             }
        }
    }
    return distance;
    
}