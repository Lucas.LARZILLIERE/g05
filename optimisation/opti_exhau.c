#include "opti_exhau.h"
#include "creation_graphe.h"
#include <math.h>
#include <limits.h>


/*
int compte_sommet(int ** Matrice)
{
    int cp =0 ;
    int len = sizeof(Matrice);
    for (int i = 0; i < len; i++)
    {
        for(int j = i ; j < len ; j++ ) // seulement besoin de parcourrire la moitie du tableau
        {
            if(Matrice[i][j]==1)
            {
                cp+=1;
            }
        }
    }
    return
}
*/

int ** floydWarshall(int ** Matrice,int taille) {
    int ** distance =(int**) malloc(taille * sizeof(int*));
    for(int i=0; i< taille;i++)
    {
        distance[i] =(int*) malloc(taille * sizeof(int));
    }
    for (int i = 0; i < taille; i++)
    {
        distance[i][i]=0;
        for (int j = i+1; j < taille; j++)
        {
            if (Matrice[i][j]!=0)
            {
                distance[i][j]=Matrice[i][j];
                distance[j][i]=Matrice[i][j];
       
                
            }
            else
            {
                distance[i][j]=INT_MAX;
                distance[j][i]=INT_MAX;
            }
            
        }
        
    }
  
    for (int k = 0; k < taille; k++)
     {
        
        for (int i = 0; i < taille; i++) {
            for (int j = 0; j < taille; j++)
             {
                if (distance[i][k] != INT_MAX && distance[k][j] != INT_MAX &&
                    distance[i][j] > distance[i][k] + distance[k][j])  
                {
                    
                    distance[i][j] = distance[i][k] + distance[k][j];
                } 
             }
        }
    }
    return distance;
    
}


int recherche_meilleur_voisin(int ** Matrice , int * Listes_des_points , int  point_actuel, int taille)
{
    int min = INT_MAX; // ici on initialise le min a max pour être sur d'avoir un voisin
    int voisin_choisie = -1 ;
    for (int i = 0; i < taille; i++)
    {
        if (Matrice[point_actuel][i] < min && (Listes_des_points[i] != 1)) // ie le voisin que n'a pas été parcouru
        {
            voisin_choisie=i;
            min=Matrice[point_actuel][i];
        }
        
    }
    return  voisin_choisie;
}



// appliquer FW avant

/*Liste_des_points [0,1,0,1] ie: on a deja parcouru le point 1 et 3 */

int recherche_glouton(int ** Matrice, int * cp ,int* distance , int  * Liste_des_points , int  point_actuel, int taille, int * ordre)
{
    if (point_actuel != -1 )
    {
        int alea = rand() %1 ;
        int voisin =0;
      
        printf("point_actuel %d\n",point_actuel);
        printf("Distance parcouru %d\n",distance[0]);
        
        voisin= recherche_meilleur_voisin(Matrice, Liste_des_points, point_actuel, taille);
        *cp+=1;
        if (voisin != -1)
        {
            ordre[*cp]=voisin;
        }
        
        

        if (alea != 1) //ie on est pas dans un cas aleatoire
        {
            *distance+=Matrice[point_actuel][voisin];
            Liste_des_points[voisin]=1;
            recherche_glouton(Matrice, cp, distance, Liste_des_points, voisin, taille, ordre);
        }
        else // cas aléatoire
        {
            alea =rand() % 10;
            while (Matrice[point_actuel][alea]!=1)
            {
                alea =rand() % 10;
            }
            *distance+=Matrice[point_actuel][voisin];
            Liste_des_points[alea]=1;
            recherche_glouton(Matrice, cp,distance, Liste_des_points, alea, taille, ordre);
        }
        
        
    }
    return 1;
}