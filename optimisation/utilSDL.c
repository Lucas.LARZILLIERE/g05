#include "utilSDL.h"

SDL_Window * initWindow(int x, int y, int w, int h, char * name) {

    // Création de fenêtre :
    SDL_Window * window = NULL;
    window = SDL_CreateWindow(
        name, // codage en utf8, donc accents possibles
        x, y,  // coin haut gauche en haut gauche de l'écran
        w, h,  // dimensions de la fenêtre
        SDL_WINDOW_RESIZABLE);

    // Si erreur dans la création de la fenêtre :
    if (window == NULL)
    {
        SDL_Log("Error : SDL window creation - %s\n",
                SDL_GetError()); // échec de la création de la fenêtre
        SDL_Quit();              // On referme la SDL
        exit(EXIT_FAILURE);
    }

    return window;
}

SDL_Renderer * initRenderer(SDL_Window * window)
{
    SDL_Renderer* renderer = NULL;

    /* Création du renderer */
    renderer = SDL_CreateRenderer(window, -1,
                                  SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL)
        end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

    return renderer;
}

void end_sdl(char ok,  char const *msg, SDL_Window *window, SDL_Renderer *renderer) { // renderer à fermer
    char msg_formated[255];
    int l;

    if (!ok)
    { // Affichage de ce qui ne va pas
        strncpy(msg_formated, msg, 250);
        l = strlen(msg_formated);
        strcpy(msg_formated + l, " : %s\n");

        SDL_Log(msg_formated, SDL_GetError());
    }

    if (renderer != NULL)
    {                                  // Destruction si nécessaire du renderer
        SDL_DestroyRenderer(renderer); // Attention : on suppose que les NULL sont maintenus !!
        renderer = NULL;
    }
    if (window != NULL)
    {                              // Destruction si nécessaire de la fenêtre
        SDL_DestroyWindow(window); // Attention : on suppose que les NULL sont maintenus !!
        window = NULL;
    }

    SDL_Quit();

    if (!ok)
    { // On quitte si cela ne va pas
        exit(EXIT_FAILURE);
    }
}

SDL_Texture * initText(SDL_Window * window, SDL_Renderer * renderer, char * text, SDL_Rect pos)
{
if (TTF_Init() < 0) end_sdl(0, "Couldn't initialize SDL TTF", window, renderer);

  TTF_Font* font = NULL;                                               // la variable 'police de caractère'
  font = TTF_OpenFont("./font.ttf", 20);                     // La police à charger, la taille désirée
  if (font == NULL) end_sdl(0, "Can't load font", window, renderer);

  TTF_SetFontStyle(font, TTF_STYLE_ITALIC);           // en italique, gras

  SDL_Color color = {20, 0, 40, 255};                                  // la couleur du texte
  SDL_Surface* text_surface = NULL;                                    // la surface  (uniquement transitoire)
  text_surface = TTF_RenderText_Blended(font, text, color); // création du texte dans la surface 
  if (text_surface == NULL) end_sdl(0, "Can't create text surface", window, renderer);

  SDL_Texture* text_texture = NULL;                                    // la texture qui contient le texte
  text_texture = SDL_CreateTextureFromSurface(renderer, text_surface); // transfert de la surface à la texture
  if (text_texture == NULL) end_sdl(0, "Can't create texture from surface", window, renderer);
  SDL_FreeSurface(text_surface);                                       // la texture ne sert plus à rien

  SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);          // récupération de la taille (w, h) du texte 
  SDL_RenderCopy(renderer, text_texture, NULL, &pos);                  // Ecriture du texte dans le renderer   
  SDL_DestroyTexture(text_texture);                                    // On n'a plus besoin de la texture avec le texte

  SDL_RenderPresent(renderer);                                         // Affichage 

  return text_texture;
}