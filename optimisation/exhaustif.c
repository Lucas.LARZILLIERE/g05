#include <stdio.h>
#include <stdlib.h>

int calculCout(int *permutation, int taille, int **matrice)
{
    int cout = 0;
    for (int i = 0; i < taille - 1; i++)
    {
        cout += matrice[permutation[i]][permutation[i + 1]];
    }
    cout += matrice[taille - 1][permutation[0]];
    return cout;
}

void genererPermutations(int nbrSommets, int taille, int **matrice, int *permutationActuelle, int *visites, int *meilleurCout, int *meilleurePermutation)
{
    int i;
    if (nbrSommets + 1 == taille)
    {                                                        // On a généré une permutation complète
        int cout = calculCout(permutationActuelle, taille, matrice); // Mettre à jour le meilleur coût et le chemin optimal
        if (cout < *meilleurCout)
        {
            *meilleurCout = cout;
            for (i = 0; i < taille; i++)
                meilleurePermutation[i] = permutationActuelle[i];
        }
    }
    else
    {
        for (int i = 0; i < taille; i++)
        {
            if (!visites[i])
            {
                visites[i] = nbrSommets + 1;                                                                        // on change l'indice du chemin déjà visité
                permutationActuelle[nbrSommets] = i;                                                                        // on met à jour le chemin actuel pour savoir ce qu'on est en train de visiter
                genererPermutations(nbrSommets + 1, taille, matrice, permutationActuelle, visites, meilleurCout, meilleurePermutation); // On continue pour le prochain sommet
                visites[i] = 0;                                                                                // Remise à zéro pour la prochaine itération
            }
        }
    }
}