#include "creation_graphe.h"


void genere(int ** matrice, int bas, int haut) {   
    if (bas < haut) {
        int k = rand() % (haut - bas) + bas + 1;
        matrice[bas][bas + 1] = 1;
        matrice[bas + 1][bas] = 1;
        if (k + 1 <= haut) {
            matrice[bas][k + 1] = 1;
            matrice[k + 1][bas] = 1;
        }
        genere(matrice, bas + 1, k);
        genere(matrice, k + 1, haut);
    }
}

void afficherMatrice(int ** matrice, int taille) {
    for (int i = 0; i < taille; i++)
    {
        for (int j = 0; j < taille; j++)
        {
            printf(" %d ", matrice[i][j]);
        }
        printf("\n");
    }
}


void genere_graphe(int ** Matrice, int p, int taille) // p entre 1 et 10
{
    for (int i = 0; i < taille; i++)
    {
        for (int j = i+1 ; j < taille; j++)
        {
            int alea =rand() %10 +1;
            if (alea < p)
            {
                Matrice[i][j]=1;
                Matrice[j][i]=1;
            }
            
        }
        
    }
}

void afficherGrapheSDL(int ** matriceEuclid, int taille, int ** coords, SDL_Window * window)
{
    // On initialise le renderer;
    SDL_Renderer * renderer;
    renderer = initRenderer(window);
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);

    // On divise la fenêtre en quadrillage
    SDL_Rect
        window_dimensions = {0}, // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
        noeud = {0}, // Correspond à un noeud du graphe
        poids = {0}, // Pour afficher le poids sur les arcs
        click = {0}; // Détecter où le joueur click
    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);   // Dimensions de la fenêtre
    int coordX = (window_dimensions.w-100) / taille;
    int coordY = (window_dimensions.h-100) / taille;

    // On trace les noeuds :
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    for (int i = 0; i < taille; i++)
    {
        noeud.x = coords[i][0];
        noeud.y = coords[i][1]; 
        // TRACER 4 TRAITS POUR CHAQUE SOMMET ET AINSI FORMER UN CARRE
        SDL_RenderDrawLine(renderer,
            coords[i][0]+20, coords[i][1]+20,   // x,y du point de la première extrémité
            coords[i][0]+40, coords[i][1]+20); // x,y seconde extrémité
        SDL_RenderDrawLine(renderer,
            coords[i][0]+40, coords[i][1]+20,   // x,y du point de la première extrémité
            coords[i][0]+40, coords[i][1]+40); // x,y seconde extrémité
        SDL_RenderDrawLine(renderer,
            coords[i][0]+40, coords[i][1]+40,   // x,y du point de la première extrémité
            coords[i][0]+20, coords[i][1]+40); // x,y seconde extrémité
        SDL_RenderDrawLine(renderer,
            coords[i][0]+20, coords[i][1]+40,   // x,y du point de la première extrémité
            coords[i][0]+20, coords[i][1]+20); // x,y seconde extrémité

        noeud.x = noeud.x +20;
        noeud.y = noeud.y +20;
        char indice[10];
        sprintf(indice, "%d", i);
        initText(window, renderer, indice, noeud);
    }

    // On trace les liens entre les noeuds :
    for (int i = 0; i < taille; i++)
    {
        for (int j = i+1; j < taille; j++)
        {
            if (matriceEuclid[i][j] != 0)
            {
                SDL_RenderDrawLine(renderer,
                    coords[i][0]+25, coords[i][1]+25,   // x,y du point de la première extrémité
                    coords[j][0]+25, coords[j][1]+25); // x,y seconde extrémité
                poids.x = (coords[i][0] + coords[j][0]) / 2;
                poids.y = (coords[i][1] + coords[j][1]) / 2;
                char p[10];
                sprintf(p, "%d", matriceEuclid[i][j]);
                initText(window, renderer, p, poids);
            }
        }
    }
    SDL_RenderPresent(renderer);

    // On permet au joueur de cliquer :
    SDL_bool
        program_on = SDL_TRUE,   // Booléen pour dire que le programme doit continuer
        event_utile = SDL_FALSE; // Booléen pour savoir si on a trouvé un event traité
    SDL_Event event;             // Evènement à traiter

    // Position de la souris
    int posX = 0;
    int posY = 0;
    int lastPoint = -1;
    while (program_on) // La boucle des évènements
    {
        event_utile = SDL_FALSE;
        while (!event_utile && SDL_PollEvent(&event))
        {
            switch (event.type){        // En fonction de la valeur du type de cet évènement                         
            case SDL_QUIT:              // Si X de la fenêtre : on quitte le programme
                program_on = SDL_FALSE; 
                event_utile = SDL_TRUE;
                break;
            case SDL_MOUSEBUTTONDOWN:           // Si clic
                if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT)) // Si clic gauche
                {
                    // On parcourt les points
                    for (int i = 0; i < taille; i++)
                    {
                        SDL_GetMouseState(&posX, &posY);
                        if (coords[i][0] + 25 > posX - 20 && coords[i][0] +25 < posX + 20) // S'il est bien positionné en X
                        {
                            if (coords[i][1] +25 > posY - 20 && coords[i][1] +25 < posY + 20) // S'il est bien positionné en Y et que c'est le premier point
                            {   
                                click.w = 20;
                                click.h = 20;
                                click.x = coords[i][0] +21;
                                click.y = coords[i][1] +21;
                                SDL_SetRenderDrawColor(renderer, 255, 127, 0, 255); // On met en orange au début
                                SDL_RenderFillRect(renderer, &click);
                                SDL_RenderPresent(renderer);
                            }
                        }
                    }
                }
                
            break;
            }
        }
    }

    
    SDL_RenderClear(renderer);
    SDL_DestroyWindow(window);
    TTF_Quit();
}

// Utile pour avoir la matrice avec coordonnées euclidiennes
void placerPoints(SDL_Window * window, int taille, int ** matrice, int ** matriceEuclid, int ** coords)
    {
        
        // Créé le noeud
        SDL_Rect noeud = {0},
                 window_dimensions = {0};
        SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);   // Dimensions de la fenêtre
        int coordX = (window_dimensions.w-100) / taille;
        int coordY = (window_dimensions.h-100) / taille;
        
        // On créé la position des noeuds :
        for (int i = 0; i < taille; i++)
        {
            int xRand = rand() % (taille);
            int yRand = rand() % (taille);
            noeud.x = xRand * coordX;
            noeud.y = yRand * coordY; 
            coords[i][0] = noeud.x+25;
            coords[i][1] = noeud.y+20;
            noeud.x = noeud.x +20;
            noeud.y = noeud.y +20;
        }

        // Créé la matrice euclidienne :
        for (int i = 0; i < taille; i++)
        {
            for (int j = i+1; j < taille; j++)
            {
                if (matrice[i][j] == 1)
                {
                    matriceEuclid[i][j] = sqrt(pow((coords[i][0] - coords[j][0]), 2) + pow((coords[i][1] - coords[j][1]), 2));
                    matriceEuclid[j][i] = sqrt(pow((coords[i][0] - coords[j][0]), 2) + pow((coords[i][1] - coords[j][1]), 2));
                }
            }
        }
        
    }