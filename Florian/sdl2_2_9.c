#include <SDL2/SDL.h>
#include <stdio.h>
#include <SDL2/SDL_image.h> 

int main(int argc, char **argv) {
    (void)argc;
    (void)argv;

    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    SDL_Window *window= NULL;
    SDL_Renderer* renderer = NULL;
    SDL_bool program_on = SDL_TRUE;
    SDL_Event event;
    SDL_DisplayMode displayMode;
    SDL_GetCurrentDisplayMode(0, &displayMode);
    SDL_Surface *image= NULL;
    SDL_Texture* texture= NULL;
    SDL_Texture* background= NULL;    
    
    window = SDL_CreateWindow("window", 0, 0, displayMode.w, displayMode.h, SDL_WINDOW_RESIZABLE);
    if (window == NULL) {
        SDL_Log("Error : SDL window creation - %s\n", SDL_GetError());
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer== NULL) {
        SDL_Log("Error : SDL window renderer creation - %s\n", SDL_GetError());
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    image = IMG_Load("./stickman_sprite_2.png");
    if (image== NULL) {
        SDL_Log("Error : SDL image creation - %s\n", SDL_GetError());
        SDL_Quit();
        exit(EXIT_FAILURE);  
    }

    texture = SDL_CreateTextureFromSurface(renderer, image);
    SDL_FreeSurface(image);
    if (texture== NULL) {
        SDL_Log("Error : SDL texture creation - %s\n", SDL_GetError());
        SDL_Quit();
        exit(EXIT_FAILURE);  
    }

    image = IMG_Load("./background.png");
    if (image== NULL) {
        SDL_Log("Error : SDL image creation - %s\n", SDL_GetError());
        SDL_Quit();
        exit(EXIT_FAILURE);  
    }

    background = SDL_CreateTextureFromSurface(renderer, image);
    SDL_FreeSurface(image);
    if (background== NULL) {
        SDL_Log("Error : SDL texture creation - %s\n", SDL_GetError());
        SDL_Quit();
        exit(EXIT_FAILURE);  
    }

    while (program_on) {
        if (SDL_PollEvent(&event)) {
            switch(event.type) {
                case SDL_QUIT:
                    program_on = SDL_FALSE;
                break;

                default:
                break;
            }
        }

        SDL_Rect source = {0}, window_dimensions = {0}, destination = {0}, state = {0}, source2 = {0}, destination2 = {0}, state2={0};

        SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
        SDL_QueryTexture(texture, NULL, NULL, &source.w, &source.h);
        SDL_QueryTexture(background, NULL, NULL, &source2.w, &source2.h);

        float zoom = 1;
        int offset_x = source.w / 70, offset_y = source.h;

        state.x = 0;
        state.y = 0;
        state.w = offset_x;
        state.h = offset_y;
        state2.x = 0;
        state2.y = 0;
        state2.w = (window_dimensions.w / window_dimensions.h) * source2.h;
        state2.h = source2.h;

        destination.w = offset_x * zoom;
        destination.h = offset_y * zoom;
        destination2.w = window_dimensions.w;
        destination2.h = window_dimensions.h;

        destination.y = window_dimensions.h - destination.h;
        destination.x = (window_dimensions.w - destination.w) / 2;
        destination2.y = 0;
        destination2.x = 0;

        int backgroundEspace = (source2.w / 2) / 70;
        for (int x = 0; x < 70; x += 1) {
            state.x += 4 * offset_x;
            state.x %= source.w;
            state2.x += backgroundEspace;
            state2.x %= source2.w / 2 + state2.w ; 
            SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
            SDL_RenderClear(renderer);
            SDL_RenderCopy(renderer, background, &state2, &destination2);
            SDL_RenderCopy(renderer, texture, &state, &destination);
            SDL_RenderPresent(renderer);
            SDL_Delay(80);
        }                         
    }
    IMG_Quit();
    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}   //gcc sdl2_2_9.c -o sdl2_2_9 -Wall -Wextra -lSDL2 -lSDL2_image
