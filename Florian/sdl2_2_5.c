#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
  (void)argc;
  (void)argv;

  SDL_Window *window_1 = NULL;
  if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());                // test d'initialisation de la SDL
    exit(EXIT_FAILURE);
  }

  SDL_DisplayMode displayMode;
  SDL_GetCurrentDisplayMode(0, &displayMode);     //récupération de la taille de l'écran
  int i;
  
  for(i=0;i<displayMode.w/50;i++){
    window_1 = SDL_CreateWindow("window",0+50*i,0+20*i,200,10,0);                   //diagonale de topleft à bottomright
    window_1 = SDL_CreateWindow("window",displayMode.w-200-50*i,0+20*i,200,10,0);   //diagonale de topright à bottomleft
  }
  
  SDL_Delay(5000);
  SDL_DestroyWindow(window_1);
  SDL_Quit();                       
  return 0;
}

//gcc sdl2_2_5.c -o sdl2_2_5 -Wall -Wextra -lSDL2