#include <SDL2/SDL.h>
#include <stdio.h>

int main(int argc, char **argv) {
    (void)argc;
    (void)argv;

    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        SDL_Log("Error : SDL initialisation - %s\n", SDL_GetError());                // test d'initialisation de la SDL
        exit(EXIT_FAILURE);
    }

    SDL_Window *window= NULL;                       //ouverture fenêtre
    SDL_Renderer* renderer = NULL;                  //ouverture render
    SDL_bool program_on = SDL_TRUE;              
    SDL_Event event;                              
    SDL_DisplayMode displayMode;
    SDL_GetCurrentDisplayMode(0, &displayMode);     //récupération de la taille de l'écran
    int xMove=0,yMove=0,red=85,green=170,blue=0;    //variable de position du carrée, de couleur 
    int iBlue=0,iRed=0,iGreen=1;                    //et de sens de l'évolution de la couleur

    
    
    
    window = SDL_CreateWindow("window", 0, 0,                                  
    displayMode.w, displayMode.h,                              
    SDL_WINDOW_RESIZABLE);     
    if (window == NULL) {
        SDL_Log("Error : SDL window creation - %s\n", SDL_GetError());                 // échec de la création de la fenêtre
        SDL_Quit();                                   
        exit(EXIT_FAILURE);
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer== NULL) {
        SDL_Log("Error : SDL window renderer creation - %s\n", SDL_GetError());                 // échec du rendu de la fenêtre
        SDL_Quit();                                
        exit(EXIT_FAILURE);
    }

    while (program_on){                           // boucle des évènements 

    if (blue==255) iBlue=1;     //changement de sens de l'évolution de la couleur
    if (blue==1) iBlue=0;
    if (red==256) iRed=1;
    if (red==1) iRed=0;
    if (green==256) iGreen=1;
    if (green==1) iGreen=0;
    if (iBlue==0) blue+=1;
    else blue-=1;
    if (iRed==0) red+=1;
    else red-=1;
    if (iGreen==0) green+=1;
    else green-=1;
    
    const Uint8* keystates = SDL_GetKeyboardState(NULL);
    if (keystates[SDL_SCANCODE_RIGHT]) {                       //gestions des mouvements
        xMove+=4;                                               //avec les touches directionnels
    }
    if (keystates[SDL_SCANCODE_LEFT]) {                    
        xMove-=4;                        
    }
    if (keystates[SDL_SCANCODE_UP]) {                    
        yMove-=4;                        
    }
    if (keystates[SDL_SCANCODE_DOWN]) {                    
        yMove+=4;                        
    }

    if (SDL_PollEvent(&event)){                 
                                            
        switch(event.type){                       
            case SDL_QUIT :                       // si on a cliqué sur la x de la fenêtre, on ferme le programme
                program_on = SDL_FALSE;                
                break;                              
            default:                           
                break;
        }
    }

    SDL_Rect rectangle;                                                
                                            
    SDL_SetRenderDrawColor(renderer,                           //création de notre rectangle            
                  red,green, blue,                             
                  255);                                 
    rectangle.x = xMove;                                             
    rectangle.y = yMove;                                                  
    rectangle.w = 100;                                                
    rectangle.h = 100;                                                
                                            
    SDL_RenderFillRect(renderer, &rectangle);
    
    SDL_RenderPresent(renderer);
    SDL_SetRenderDrawColor(renderer,                                       
                0, 0, 0,                            
                255); 
    SDL_RenderClear(renderer);
    SDL_Delay(50);
    } 
    SDL_DestroyRenderer(renderer);  
    SDL_DestroyWindow(window);
    SDL_Quit();                              
    return 0;
}   //gcc sdl2_2_7.c -o sdl2_2_7 -Wall -Wextra -lSDL2