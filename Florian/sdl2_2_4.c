SDL_bool program_on = SDL_TRUE;               // Booléen pour dire que le programme doit continuer
SDL_Event event;                              // c'est le type IMPORTANT !!

while (program_on){                           // Voilà la boucle des évènements 


    if (SDL_PollEvent(&event)){                 // si la file d'évènements n'est pas vide : défiler l'élément en tête
                                                // de file dans 'event'
switch(event.type){                       // En fonction de la valeur du type de cet évènement
case SDL_QUIT :                           // Un évènement simple, on a cliqué sur la x de la fenêtre
    program_on = SDL_FALSE;                 // Il est temps d'arrêter le programme
    break;

default:                                  // L'évènement défilé ne nous intéresse pas
    break;
}
    }
    // Affichages et calculs souvent ici
    }  