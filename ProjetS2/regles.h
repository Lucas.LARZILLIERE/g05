#include <math.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include "utilMatrice.h"
#include "matrice_jeu.h"

#define NB_REGLES 10

// Directions pour déterminer le contenu de la case
typedef enum {
    JOKER=-1, PASOBSTACLE, OBSTACLE, BONUS
} case_t;

// Directions pour déterminer position du joueur
typedef enum {
    JOKE=-1, N, S, O, E
} direction;

// Actions possibles d'un joueur
typedef enum {
    HAUT, BAS, GAUCHE, DROITE
} action_t;

// Structure d'une règle
typedef struct {
    case_t devant, derrière, gauche, droite;   // 4 cases voisines
    direction directionJoueur1;                 // Direction du premier Joueur
    direction directionJoueur2;                 // Direction du deuxième joueur

    int priorite;                              // Priorité de la règle
    action_t action;                           // Action choisie dans ce cas
} Regle;

// Position de quelquechose sur la map
typedef struct {
    int x, y; // Positions dans l'espace
} position;


Regle * initRegles(); // Initialise toutes les règles du monstre et du joueur
int GetDirection(position a, position b); // Récupère la direction (N, S, O, E) la plus pertinente d'où est b par rapport à a
int * GetCasesAutour(int ** jeu, position a); // Récupère les cases autour et les renvoie dans un tableau d'entiers
int GetPointLePlusProche(int points[100][2], position a, int pointsRestants);
Regle *FiltrageRegles(Regle *regles, int *cases, int directionJoueur1, int directionJoueur2, int *nbRegles);
int GetProximite(position a, position b);
Regle choix_regle(Regle * LesRegles, int nbRegles);