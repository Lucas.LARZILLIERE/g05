#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL.h>
#include "utilSDL.h"
#include "utilMatrice.h"

#define max(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })

#define min(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

#define DIM_SPRITE_STATIC 16
#define DIM_SPRITE_ANIM 24
#define DIM_SPRITE_ICON 50

//création du tableau des rectangles sources qu'on va utiliser sur le staticTexture
//[0]:herbe_1
//[1]:herbe_2
//[2]:arbre_vert_foncé
//[3]:lingot_or
//[4]:poulailler
//[5]:buisson_baie_violet
//[6]:rocher
//[7]:herbe_limite_menu
//[8]:herbe_menu
SDL_Rect * create_tabStaticSpriteSource();

//création du tableau des rectangles sources qu'on va utiliser sur le gameIconsTexture
//[0]:3_barres_horizont.
SDL_Rect * create_tabGameIconSpriteSource();

//création du tableau des 3 sprites animés des animaux
SDL_Texture** create_tabAnimTexture(SDL_Window* window, SDL_Renderer* renderer);

//Explicite
void destroy_tabAnimTexture(SDL_Texture** tabAnimSpriteTexture);

//Dessine l'arrière plan du jeu
void draw_background(SDL_Texture *my_texture,SDL_Rect * tabStaticSpriteSource, SDL_Renderer *renderer,int windowDim,int ** matrice,int taille);

//Dessine tout les objets statique du jeu
void draw_static(SDL_Texture *my_texture,SDL_Rect * tabStaticSpriteSource, SDL_Renderer *renderer,int windowDim,int ** matrice,int taille);

//Dessine tous les joueurs du jeu
void draw_player(SDL_Renderer * renderer, SDL_Texture ** tabAnimSpriteTexture,int ** matrice, int taille , int windowDim, int etape);

// Dessine le menu pause lors de la pause du jeu
void draw_menuPause(SDL_Renderer * renderer, SDL_Texture * menuPause, int taille , int windowDim);

// Dessine le tableau des scores du jeu
void draw_scores(SDL_Renderer* renderer, SDL_Texture** tabAnimSpriteTexture,int tabScoreDim, int windowDim);

// Ecris les scores du jeu
void write_scores(SDL_Renderer* renderer, int tabScoreDim, int windowDim, char **);

// Ecris le sous-titre du menu
void write_menuSubTitle(SDL_Renderer* renderer,int windowDim,int tabScoreDim);

// Dessine l'herbe sur le menu
void draw_menuHerbe(SDL_Texture *my_texture,SDL_Rect * tabStaticSpriteSource, SDL_Renderer *renderer,int windowDim,int tabScoreDim);

// Dessine les boutons en jeu et en crédits (avec l'animation) et renvoie leur zone dans la fenêtre
SDL_Rect draw_jeuBoutons(SDL_Renderer * renderer, SDL_Texture * gameIconsTexture,SDL_Rect* tabGameIconSpriteSource,SDL_bool gameBoutonMenuBG, int tabScoreDim , int windowDim);

// Dessine le titre dans le menu
float draw_menuTitreAnim(SDL_Texture *my_texture, SDL_Renderer *renderer,int windowDim,int tabScoreDim, float coef,int statut);

// Dessine et anime les nuages du menu
void draw_cloudAnim(SDL_Texture *my_texture, SDL_Renderer *renderer,int etapeMenu ,int windowDim,int tabScoreDim,int randNumber[3]);

// Desine les 3 boutons (avec leurs animations) dans le menu et renvoie
SDL_Rect * draw_menuBoutons(SDL_Renderer * renderer,SDL_Texture ** tabTexture,SDL_bool * tabSourisSurBouton,SDL_Rect * tabBoutonRect, int windowDim,int tabScoreDim);

// Desine les crédits
void draw_credits(SDL_Texture *my_texture, SDL_Renderer *renderer,int windowDim,int tabScoreDim);

// Dessine les animaux dans le menu
void draw_menuPlayer(SDL_Renderer* renderer, SDL_Texture** tabAnimSpriteTexture, int windowDim,int tabScoreDim, int etape);