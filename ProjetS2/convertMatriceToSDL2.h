#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include "utilMatrice.h"

//converti la matrice des objets statique en numéro de rectangle source
//voir create_tabStaticSpriteSource() dans SDL2.h pour plus de détails
int** convert_MatriceStatic(int** matrice, int taille);

//converti la matrice des objets en mouvement en type et position de joueur
//voir create_tabStaticSpriteSource() dans SDL2.h pour plus de détails
int** convert_MatricePlayer(int** matrice, int taille);

//créer la matrice du fond d'herbe
int** create_MatriceBG( int taille);

//crée un tabbleau de chaîne de caractère
char** allouerTableauChaine(int taille);

// Explicite
void intToChar_TabScore(char ** tableau, int pointsPoule, int pointsRenard, int pointsVipere);

// Explicite
void libererTableauChaine(char** tableau, int taille);

