#include "utilSDL.h"
#include "matrice_jeu.h"
#include "utilMatrice.h"
#include "SDL2.h"
#include "convertMatriceToSDL2.h"
#include "optimisationBasique.h"
#include <time.h>
#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>


int main()
{
    /*====== Déclaration des variables SDL2 ========*/

        SDL_Window* window = NULL;                  // Contenu de la fenêtre
        SDL_Renderer* renderer = NULL;              // Contenu du rendu
        SDL_bool    program_on = SDL_TRUE,          // Booléen pour dire que le programme doit continuer
                    paused = SDL_FALSE,             // Booléen pour dire que le programme est en pause
                    event_utile = SDL_FALSE,        // Booléen pour savoir si on a trouvé un event traité 
                *   tabEtat=init_tabSDL_bool(4),    // Tableau de booléen pour savoir dans quel état le programme se trouve
                //[0]:Menu
                //[1]:Jouer
                //[2]:Crédits
                *   tabSourisSurBouton=init_tabSDL_bool(5); // Tableau de booléen pour savoir si la souris se trouve sur un bouton           
                //[0]:Etat crédits & jeu : 'retour menu'
                //[1]:Etat menu : 'jouer'
                //[2]:Etat menu : 'crédits'
                //[3]:Etat menu : 'quitter'
        SDL_Event event;                            // Variable de tout les évènements
        SDL_Texture **  tabTexture;                 // Tableau des textures
        char * cheminImages[15]= {                  // Listé ci dessous
            "./sprite/roguelikeSheet_transparent.png",  //[0]
            "./sprite/sheet_white1x.png",               //[1]
            "./sprite/chicken.png",                     //[2]
            "./sprite/fox.png",                         //[3]
            "./sprite/snake.png",                       //[4]
            "./sprite/pauseTexture.png",                //[5]
            "./sprite/Clouds_V2.png",                   //[6]
            "./sprite/titre.png",                       //[7]
            "./sprite/jouerTextureClair.png",           //[8]
            "./sprite/jouerTexture.png",                //[9]
            "./sprite/creditsTextureClair.png",         //[10]
            "./sprite/creditsTexture.png",              //[11]
            "./sprite/quitterTextureClair.png",         //[12]
            "./sprite/quitterTexture.png",              //[13]
            "./sprite/credits.png"};                    //[14]
        SDL_Rect *  tabBoutonRect = init_tabSDL_Rect(5),    // Tableau des rectangles de chaque bouton
                //[0]:Etat crédits & jeu : 'retour menu'
                //[1]:Etat menu : 'jouer'
                //[2]:Etat menu : 'crédits'
                //[0]:Etat menu : 'quitter'
                *   tabGameIconSpriteSource,        // Tableau des rectangles sources des différents sprite de la texture icones du jeu
                *   tabStaticSpriteSource;          // Tableau des rectangles sources des différents sprite de la texture principale 


    /*====== Déclaration des variables standard ========*/

        int     cp_tresor=0,            // Nb de bonus
                taille=30,              // Taille du carré du plateau
                mouseX,                 // Position en X de la souris
                mouseY,                 // Position en Y de la souris
                randNumber[3],          // Un ensemble de 3 numéro random
                poule[2],                           
                vipere[2],
                renard[2],
                points[100][2],
                banniereScoreDim=100,               // Taille de la bannière des scores
                etape=1,                            // Utile pour les animations
                statut=0,                           // Utile pour l'animation du titre du menu
            **  matrice = initMatrice(taille),      // Matrice composé uniquement du monde sans joueur
            **  monde_dep = initMatrice(taille),    // Matrice complète du jeu 
            **  SDL_matriceStatic,                  // Matrice monde_dep décomposé en obstacle reconaissable par la SDL
            **  SDL_matriceBG,                      // Matrice du fond de la SDL
            **  SDL_matricePlayer,                  // Matrice monde_dep décomposé en joueur reconaissable par la SDL
                windowDim=660,                      // Taille du moins long bord de la fenêtre
                pointsPoule = 0,
                pointsRenard = 0,
                pointsVipere = 0,
                spawnPoule[2],
                spawnRenard[2],
                spawnVipere[2];
        position    poulePos,
                    renardPos,
                    viperePos;
        Regle * r1=initRegles();
        Regle * r2=initRegles();
        Regle * r3=initRegles();
        char**  tabScore = allouerTableauChaine(4); // Tableau des scores en chaîne de caractère
        float coef=0.8;                             // Coefficient pour l'animation du titre

    /*====== Initialisation standards ========*/

        srand(time(NULL));      // Init pour le random
        /*   
        Regle *nouvellesRegles = trouver_le_meilleur_cerveau(10,10000,matrice, monde_dep, &poulePos, &renardPos, &viperePos, r1, r2, r3, spawnPoule, spawnRenard, spawnVipere);
        cp_tresor = 0;
        pointsPoule = 0;
        pointsRenard = 0;
        pointsVipere = 0;
        memcpy(r1, nouvellesRegles, sizeof(Regle) * NB_REGLES);
        memcpy(r2, nouvellesRegles, sizeof(Regle) * NB_REGLES);
        memcpy(r3, nouvellesRegles, sizeof(Regle) * NB_REGLES);
        afficherRègles(r1, r2, r3);
        */
        
        matrice = creation_du_monde(matrice, &cp_tresor, taille, points, spawnPoule, spawnRenard, spawnVipere); // Matrice composé uniquement du monde sans joueur
        placement_joueur(monde_dep, matrice, taille, poule, renard, vipere);  

        poulePos.x = poule[0];
        poulePos.y = poule[1];
        renardPos.x = renard[0];
        renardPos.y = renard[1];
        viperePos.x = vipere[0];
        viperePos.y = vipere[1];

        randNumber[0]=rand();       // Init des nombres random
        randNumber[1]=rand();
        randNumber[2]=rand();

        afficherRègles(r1, r2, r3);

    /*====== Initialisation pour la SDL ========*/
        SDL_matriceStatic = convert_MatriceStatic(monde_dep, taille);   // On créer la matrice des objets statique
        SDL_matriceBG = create_MatriceBG(taille);                       // On créer la matrice du fond du jeu
        SDL_matricePlayer = convert_MatricePlayer(monde_dep, taille);   // On créer la matrice de position des joueurs

        if (SDL_Init(SDL_INIT_VIDEO) != 0)
            end_sdl(0, "ERROR SDL INIT", window, renderer);                 // Gestion init SDL + retour erreur
        if (TTF_Init() < 0)
            end_sdl(0, "Couldn't initialize SDL TTF", window, renderer);    // Gestion init TTF + retour erreur

        window = initWindow(0, 0, windowDim+banniereScoreDim, windowDim, "Un Super Jeu !"); // création d'une fenêtre
        renderer = initRenderer(window);                                                    //création du rendu   
        
        tabStaticSpriteSource = create_tabStaticSpriteSource();         //chargement du Tableau des rectangles sources de la texture [1]
        tabTexture= create_tabTexture(window, renderer,cheminImages);   // chargement du Tableau des textures
        tabGameIconSpriteSource=create_tabGameIconSpriteSource();       //chargement du Tableau des rectangles sources de la texture [2]
        
        tabEtat[0]=SDL_TRUE;    //Passage du programme dans l'état menu

    /*====== Boucle des évènements ========*/

        while (program_on) {
            event_utile = SDL_FALSE;
            tabSourisSurBouton=reset_tabSDL_bool(tabSourisSurBouton, 5);                                            // Remise à FAUX            
            tabSourisSurBouton=update_tabSourisSurBouton(tabEtat,tabSourisSurBouton,tabBoutonRect,mouseX,mouseY);   // Et mise à jour
            while(!event_utile && SDL_PollEvent(&event)) {      // Tant qu' on n'a pas trouvé d'évènement utile
                                                                // et la file des évènements stockés n'est pas vide et qu'on n'a pas
                                                                // terminé le programme Défiler l'élément en tête de file dans 'event'

            /*====== Evènements en jeu ========*/

                if ((!tabEtat[2])&(tabEtat[1])&(!tabEtat[0])){  // Si le programme est dans l'état jeu
                    switch (event.type){                        // En fonction de la valeur du type de cet évènement

                    /*====== Evènements autres ========*/

                        case SDL_QUIT:              // Un évènement simple, on a cliqué sur la x de la // fenêtre
                            program_on = SDL_FALSE; // Il est temps d'arrêter le programme
                            event_utile = SDL_TRUE;
                            break;

                    /*====== Evènements de touche appuyés ========*/
                    
                        case SDL_KEYDOWN:       // Le type de event est : une touche appuyée 
                            switch (event.key.keysym.sym) { // la touche appuyée est ...
                                case SDLK_p:                // 'p'
                                case SDLK_SPACE:            // ou 'SPC'
                                    paused = !paused;       // basculement pause/unpause
                                    event_utile = SDL_TRUE;
                                    break;
                                case SDLK_ESCAPE:           // 'ESCAPE'            
                                    program_on = SDL_FALSE; // Il est temps d'arrêter le programme
                                    event_utile = SDL_TRUE;
                                    break;
                                case SDLK_m:                //'m'
                                    tabEtat[1]=SDL_FALSE;   // on retourne au menu
                                    tabEtat[0]=SDL_TRUE;
                                    tabEtat[2] = SDL_FALSE; 
                                    paused=SDL_FALSE;
                                    etape=0;
                                    coef=0.8;          
                                    event_utile = SDL_TRUE;
                                default:                    // Une touche appuyée qu'on ne traite pas
                                    break;
                            }
                            break;

                    /*====== Evènements de souris ========*/
                        
                        case SDL_MOUSEBUTTONDOWN:                                           // Click souris   
                            if ( tabSourisSurBouton[0] & SDL_BUTTON(SDL_BUTTON_LEFT) ) {    // Si c'est un click gauche dans la zone du bouton retour menu
                                tabEtat[1]=SDL_FALSE;                                       // on retourne au menu
                                tabEtat[0]=SDL_TRUE;
                                tabEtat[2] = SDL_FALSE; 
                                paused=SDL_FALSE;
                                etape=0;
                                coef=0.8;          
                                event_utile = SDL_TRUE;
                            }
                            break;
                        case SDL_MOUSEMOTION:                       // Si la souris bouge
                            SDL_GetMouseState(&mouseX, &mouseY);    // On actualise sa position
                            break;

                    /*====== Les évènements qu'on n'a pas envisagé ========*/

                        default:
                            break;
                    }
                }

            /*====== Evènements en menu ========*/

                else if ((!tabEtat[2])&(!tabEtat[1])&(tabEtat[0])){ // Si le programme est dans l'etat menu
                    switch (event.type){                            // En fonction de la valeur du type de cet évènement

                        /*====== Evènements autres ========*/

                            case SDL_QUIT:              // Un évènement simple, on a cliqué sur la x de la // fenêtre
                                program_on = SDL_FALSE; // Il est temps d'arrêter le programme
                                event_utile = SDL_TRUE;
                                break;

                        /*====== Evènements de touche appuyés ========*/
                        
                            case SDL_KEYDOWN:                       // Le type de event est : une touche appuyée
                                switch (event.key.keysym.sym) {     // la touche appuyée est ...
                                    case SDLK_j:                    // 'j'            
                                        tabEtat[1] = SDL_TRUE;      // on passe en jeu
                                        tabEtat[0] = SDL_FALSE;
                                        tabEtat[2] = SDL_FALSE;                
                                        event_utile = SDL_TRUE;
                                        etape=0;
                                        break;
                                    case SDLK_c:                    // 'c'            
                                        tabEtat[1] = SDL_FALSE;     // on passe en crédits
                                        tabEtat[0] = SDL_FALSE;
                                        tabEtat[2] = SDL_TRUE;              
                                        event_utile = SDL_TRUE;
                                        etape=0;
                                        break;
                                    case SDLK_ESCAPE:               // 'ESCAPE' 
                                    case SDLK_q:                    // ou 'q'
                                        program_on = SDL_FALSE;     // Il est temps d'arrêter le programme
                                        event_utile = SDL_TRUE;
                                        break;
                                    default:                        // Une touche appuyée qu'on ne traite pas
                                        break;
                                }
                                break;

                        /*====== Evènements de souris ========*/

                            case SDL_MOUSEBUTTONDOWN:                                       // Click souris   
                                if (tabSourisSurBouton[1] & SDL_BUTTON(SDL_BUTTON_LEFT) ) { // Si c'est un click gauche dans la zone du bouton jouer
                                    tabEtat[1]=SDL_TRUE;                                    // on rentre dans le jeu
                                    tabEtat[0]=SDL_FALSE; 
                                    tabEtat[2] = SDL_FALSE;     
                                    etape=0;    
                                    event_utile = SDL_TRUE;
                                }
                                else if (tabSourisSurBouton[3] & SDL_BUTTON(SDL_BUTTON_LEFT) ) {    // Si c'est un click gauche dans la zone du bouton quitter
                                    program_on = SDL_FALSE;                                         // Il est temps d'arrêter le programme
                                    event_utile = SDL_TRUE;
                                }
                                else if (tabSourisSurBouton[2] & SDL_BUTTON(SDL_BUTTON_LEFT) ) {    // Si c'est un click gauche dans la zone du bouton crédits
                                    tabEtat[1] = SDL_FALSE;                                         // on rentre dans les crédits
                                    tabEtat[0] = SDL_FALSE;
                                    tabEtat[2] = SDL_TRUE;              
                                    event_utile = SDL_TRUE;
                                    etape=0;
                                }
                                break;
                        case SDL_MOUSEMOTION:                       // Si il y a un mouvement de souris
                            SDL_GetMouseState(&mouseX, &mouseY);    // Alors on actualise sa position
                            break;
                        
                        /*====== Les évènements qu'on n'a pas envisagé ========*/

                            default:
                                break;
                    }
                }

            /*====== Evènements en crédits ========*/

                else if ((tabEtat[2])&(!tabEtat[1])&(!tabEtat[0])){     // Si le programme est dans l'état crédits
                    switch (event.type){                                // En fonction de la valeur du type de cet évènement

                        /*====== Evènements autres ========*/

                            case SDL_QUIT:              // Un évènement simple, on a cliqué sur la x de la // fenêtre
                                program_on = SDL_FALSE; // Il est temps d'arrêter le programme
                                event_utile = SDL_TRUE;
                                break;

                        /*====== Evènements de touche appuyés ========*/
                        
                            case SDL_KEYDOWN:       // Le type de event est : une touche appuyée
                                switch (event.key.keysym.sym) {     // la touche appuyée est ...   
                                    case SDLK_ESCAPE:               // 'ESCAPE' 
                                        program_on = SDL_FALSE;     // Il est temps d'arrêter le programme
                                        event_utile = SDL_TRUE;
                                        break;
                                    case SDLK_m:                    // 'm'
                                        tabEtat[1]=SDL_FALSE;       // on retourne au menu
                                        tabEtat[0]=SDL_TRUE;
                                        tabEtat[2] = SDL_FALSE; 
                                        paused=SDL_FALSE;
                                        etape=0;
                                        coef=0.8;          
                                        event_utile = SDL_TRUE;
                                        break;
                                    default:                        // Une touche appuyée qu'on ne traite pas
                                        break;
                                }
                                break;

                        /*====== Evènements de souris ========*/
                        
                            case SDL_MOUSEBUTTONDOWN:                                       // Click souris   
                                if (tabSourisSurBouton[0] & SDL_BUTTON(SDL_BUTTON_LEFT) ) { // Si c'est un click gauche dans la zone du bouton retour menu
                                    tabEtat[1]=SDL_FALSE;                                   // on retourne au menu
                                    tabEtat[0]=SDL_TRUE;
                                    tabEtat[2] = SDL_FALSE; 
                                    paused=SDL_FALSE;
                                    etape=0;
                                    coef=0.8;          
                                    event_utile = SDL_TRUE;
                                }
                                break;
                        case SDL_MOUSEMOTION:                       // Si il y a un mouvement de souris
                            SDL_GetMouseState(&mouseX, &mouseY);    // Alors on actualise sa position
                            break;

                        /*====== Les évènements qu'on n'a pas envisagé ========*/

                            default:
                                break;
                    }
                }

            /*====== Evènements en état non envisagé ========*/

                else
                {
                    program_on = SDL_FALSE; // Il est temps d'arrêter le programme
                    event_utile = SDL_TRUE;
                }
            }

            /*====== Actions en jeu ========*/

                if ((!tabEtat[2])&(tabEtat[1])&(!tabEtat[0])) {     // Si le programme est dans l'état jeu

                /*====== Actions normal ========*/ 

                    if (!paused) {                                              // Effectue les actions suivantes si pas en pause
                        SDL_SetRenderDrawColor(renderer,55, 170, 165,255);  
                        SDL_RenderClear(renderer);                              // Clean le render avec la couleur bleu cair

                        draw_background(tabTexture[0],tabStaticSpriteSource, renderer, windowDim,SDL_matriceBG, taille);    // Dessine le fond en herbe
                        draw_static(tabTexture[0],tabStaticSpriteSource, renderer, windowDim,SDL_matriceStatic, taille);    // Dessine els objets statiques
                        draw_player(renderer, tabTexture, SDL_matricePlayer, taille , windowDim, etape);                    // Dessine les joueurs
                        draw_scores(renderer, tabTexture, banniereScoreDim, windowDim);                                     // Dessine l'icône des joueurs pour les scores
                        write_scores(renderer, banniereScoreDim, windowDim, tabScore);                                      // Ecrit les scores
                        tabBoutonRect[0]=draw_jeuBoutons(renderer, tabTexture[1],tabGameIconSpriteSource,                   // Dessine et anime le bouton retour menu
                                                        tabSourisSurBouton[0],banniereScoreDim , windowDim);
                        SDL_RenderPresent(renderer);                                                                    
                        etape+=1;                   // Actualise pour l'animation des joueurs
                        if (etape==4) etape=0;      

                        tour(matrice, monde_dep, &poulePos, &renardPos, &viperePos, r1, r2, r3,&pointsPoule,    // On effectue un tour de jeu
                        &pointsRenard,&pointsVipere,spawnPoule,spawnRenard,spawnVipere, points, &cp_tresor);
                        
                        SDL_matricePlayer=convert_MatricePlayer(monde_dep, taille);                 // Actualisation des joueurs
                        SDL_matriceStatic=convert_MatriceStatic(monde_dep,  taille);                // Actualisations des objets statiques
                        intToChar_TabScore(tabScore, pointsPoule, pointsRenard, pointsVipere);      // Actualisation des scores
                    }

                /*====== Actions en pause ========*/  

                    else {                                                                                  // Effectue les actions suivantes si en pause
                        draw_menuPause(renderer, tabTexture[5], taille , windowDim);                        // Dessine le menu pause
                        tabBoutonRect[0]=draw_jeuBoutons(renderer, tabTexture[1],tabGameIconSpriteSource,   // Dessine et anime le bouton retour menu
                                                    tabSourisSurBouton[0],banniereScoreDim , windowDim);
                        SDL_RenderPresent(renderer);
                    }
                }

            /*====== Actions en menu ========*/

                else if ((!tabEtat[2])&(!tabEtat[1])&(tabEtat[0])) {                                            // Si le programme est dans l'état jeu
                    SDL_SetRenderDrawColor(renderer,55, 170, 165,255);
                    SDL_RenderClear(renderer);                                                                  // Clean le render avec la couleur bleu cair
                    draw_menuHerbe(tabTexture[0],tabStaticSpriteSource, renderer,windowDim,banniereScoreDim);   // Dessine l'herbe
                    draw_menuPlayer(renderer, tabTexture, windowDim,banniereScoreDim, etape);                   // Dessine et anime les joueurs
                    tabBoutonRect=draw_menuBoutons(renderer,tabTexture,tabSourisSurBouton,                      // Dessine et anime les boutons
                                                tabBoutonRect , windowDim,banniereScoreDim);
                    draw_cloudAnim(tabTexture[6], renderer,etape ,windowDim,banniereScoreDim,randNumber);       // Dessine et anime les nuages
                    write_menuSubTitle(renderer,windowDim,banniereScoreDim);                                    // Ecrit le sous-titre
                    coef=draw_menuTitreAnim(tabTexture[7], renderer,windowDim,banniereScoreDim,coef,statut);    // Dessine et anime le titre
                    SDL_RenderPresent(renderer);                                          

                    etape+=1;                                           // Actualise pour l'animation des nuages
                    if (etape==(windowDim+banniereScoreDim)) etape=0;
                    if ((coef>0.85)&(statut==0))  statut=1;             // Actualise pour l'animation du titre
                    if ((coef<0.8)&(statut==1))  statut=0; 
                }

            /*====== Actions en crédits ========*/

                else if ((tabEtat[2])&(!tabEtat[1])&(!tabEtat[0])) {                                    // Si le programme est en crédits
                    SDL_SetRenderDrawColor(renderer,55, 170, 165,255);
                    SDL_RenderClear(renderer);                                                          // Clean le render avec la couleur bleu cair
                    draw_credits(tabTexture[14], renderer,windowDim,banniereScoreDim);                  // Dessine les crédits
                    tabBoutonRect[0]=draw_jeuBoutons(renderer, tabTexture[1],tabGameIconSpriteSource,   // Dessine et anime le bouton retour menu
                                                tabSourisSurBouton[0],banniereScoreDim , windowDim);
                    SDL_RenderPresent(renderer);
                }

            /*====== Actions en état non-envisagé ========*/

                else{                           // Si le programme est dans aucun état
                    program_on = SDL_FALSE;     // Il est temps d'arrêter le programme
                }           
        SDL_Delay(50); // Petite pause entre chaque boucle
        }

    /*====== Libérations de la SDL ========*/

        destroy_tabTexture(tabTexture);
        free(tabEtat);                         
        free(tabSourisSurBouton);
        free(tabBoutonRect);
        free(tabGameIconSpriteSource);
        free(tabStaticSpriteSource);
        end_sdl(1, "Normal ending", window, renderer);      // Fermeture de la SDL

    /*====== Libérations standards ========*/

        freeMatrice(matrice, taille);
        freeMatrice(monde_dep, taille);
        freeMatrice(SDL_matriceStatic, taille);
        freeMatrice(SDL_matriceBG, taille);
        freeMatrice(SDL_matricePlayer, taille);
        libererTableauChaine(tabScore, 4);
    return EXIT_SUCCESS;
}
