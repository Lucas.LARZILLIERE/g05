#include "Rules_change.h"

void tour(int **layerFixe, int **layerDynamique, position *j1, position *j2, position *j3, Regle *r1, Regle *r2, Regle *r3, int *pointsPoule, int *pointsRenard, int *pointsVipere, int spawnPoule[2], int spawnRenard[2], int spawnVipere[2], int points[100][2], int * cp_tresor);
void verifTresors(int **layerFixe, position j, int *pointsJoueur, int * cp_tresor);
void changerPositionJoueur(int **layerFixe, int **layerDynamique, position * j, int jNbr, int dir);
void verifJoueurs(int **layerFixe, int **layerDynamique, position *poule, position *renard, position *vipere, int *pointsPoule, int *pointsRenard, int *pointsVipere);
