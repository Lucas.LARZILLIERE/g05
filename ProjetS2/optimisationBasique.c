#include "optimisationBasique.h"

void afficherRègles(Regle *r1, Regle *r2, Regle *r3)
{
    // Affichage règles poule
    printf("Poule : \n");
    for (int i = 0; i < NB_REGLES; i++)
    {
        printf("%d, %d, %d, %d, %d, %d, %d, %d\n", r1[i].devant, r1[i].derrière, r1[i].gauche, r1[i].droite, r1[i].directionJoueur1, r1[i].directionJoueur2, r1[i].priorite, r1[i].action);
    }
    printf("\n");

    // Affichage règles renard
    printf("Renard : \n");
    for (int i = 0; i < NB_REGLES; i++)
    {
        printf("%d, %d, %d, %d, %d, %d, %d, %d\n", r2[i].devant, r2[i].derrière, r2[i].gauche, r2[i].droite, r2[i].directionJoueur1, r2[i].directionJoueur2, r2[i].priorite, r2[i].action);
    }
    printf("\n");

    // Affichage règles vipere
    printf("Vipere : \n");
    for (int i = 0; i < NB_REGLES; i++)
    {
        printf("%d, %d, %d, %d, %d, %d, %d, %d\n", r3[i].devant, r3[i].derrière, r3[i].gauche, r3[i].droite, r3[i].directionJoueur1, r3[i].directionJoueur2, r3[i].priorite, r3[i].action);
    }
    printf("\n");
}



void partie(int **layerFixe, int **layerDynamique, position *j1, position *j2, position *j3,
            Regle *r1, Regle *r2, Regle *r3,
            int spawnPoule[2], int spawnRenard[2], int spawnVipere[2],
            int *bestPoints,
            int posPoule[2], int posRenard[2], int posVipere[2]
            )
{
    // On réinitialise les points
    int pointsPoule = 0;
    int pointsRenard = 0;
    int pointsVipere = 0;
    *bestPoints=0;

    // On regénère la map dynamique
    layerFixe = initMatrice(30);
    layerDynamique = initMatrice(30);

    int points[100][2];
    int pointsRestants = 0;

    layerFixe = creation_du_monde(layerFixe, &pointsRestants, 30, points, spawnPoule, spawnRenard, spawnVipere);
    placement_joueur(layerDynamique, layerFixe, 30, posPoule, posRenard, posVipere);
    (*j1).x = posPoule[0];
    (*j1).y = posPoule[1];
    (*j2).x = posRenard[0];
    (*j2).y = posRenard[1];
    (*j3).x = posVipere[0];
    (*j3).y = posVipere[1];

    for (int j = 0; j < 100; j++) // Chaque partie fait 100 tours
    {
        tour(layerFixe, layerDynamique, j1, j2, j3, r1, r2, r3, &pointsPoule, &pointsRenard, &pointsVipere, spawnPoule, spawnRenard, spawnVipere, points, &pointsRestants);
    }
    // if(pointsPoule>pointsRenard && pointsPoule>pointsVipere)
    // {
    //     *bestPoints=pointsPoule;
    // }
    //   if(pointsRenard>pointsPoule && pointsRenard>pointsVipere)
    // {
    //     *bestPoints=pointsPoule;
    // }
    //   if(pointsVipere>pointsRenard && pointsVipere>pointsPoule)
    // {
    //     *bestPoints=pointsPoule;
    // }
  *bestPoints=*bestPoints+pointsPoule+pointsRenard+pointsVipere;
}

float moyenneParties(int nbParties, int **layerFixe, int **layerDynamique, position *j1, position *j2, position *j3,
                   Regle *r1, Regle *r2, Regle *r3,
                   int spawnPoule[2], int spawnRenard[2], int spawnVipere[2],
                   int *bestPoints,
                   int posPoule[2], int posRenard[2], int posVipere[2])
{
    int total = 0;
    for (int j = 0; j < nbParties; j++)
    {
        partie(layerFixe, layerDynamique, j1, j2, j3, r1, r2, r3,
               spawnPoule, spawnRenard, spawnVipere, bestPoints,posPoule, posRenard, posVipere);
        total += *bestPoints;
    }

    return ((float)total )/ nbParties;
}




Regle *  trouver_le_meilleur_cerveau(int nombre_partie, int nombre_iteration,int **layerFixe, int **layerDynamique, position *j1, position *j2, position *j3, Regle *r1, Regle *r2, Regle *r3, int spawnPoule[2], int spawnRenard[2], int spawnVipere[2])
{
      // Initialisations :
    int bestPoints = 0;
    int posPoule[2];
    int posRenard[2];
    int posVipere[2];
    int **matrice_alea = initMatrice(NB_REGLES);
    float moyenne=0;

    // Compteurs :
    int compteurligne = 0;
    int compteurcolone = 0;
    // int compteur_changement=0;

    // Nouvelles règles :
    Regle *newr1 = (Regle *)malloc(sizeof(Regle) * NB_REGLES);
    memcpy(newr1, r1, sizeof(Regle) * NB_REGLES);
    Regle *newr2 = (Regle *)malloc(sizeof(Regle) * NB_REGLES);
    memcpy(newr2, r2, sizeof(Regle) * NB_REGLES);
    Regle *newr3 = (Regle *)malloc(sizeof(Regle) * NB_REGLES);
    memcpy(newr3, r3, sizeof(Regle) * NB_REGLES);
    for (int i = 0; i < nombre_iteration; i++)
    {
        
        float nouvelle_moyenne=moyenneParties( nombre_partie,  layerFixe, layerDynamique,j1, j2, j3,
                    newr1, newr2, newr3,
                    spawnPoule,  spawnRenard,  spawnVipere,
                    &bestPoints,
                    posPoule, posRenard,posVipere
                    );

        if (nouvelle_moyenne>moyenne)
        {
            moyenne=nouvelle_moyenne;
            memcpy(r1, newr1, sizeof(Regle) * NB_REGLES);
            memcpy(r2, newr2, sizeof(Regle) * NB_REGLES);
            memcpy(r3, newr3, sizeof(Regle) * NB_REGLES);
        }
        


        if (i % (NB_REGLES * 9) == 0)
        {
            matrice_alea = matrice_changements(matrice_alea);
        }

        if (compteurcolone == NB_REGLES)
        {
            compteurcolone = 0;
            compteurligne += 1;
        }

        if (compteurligne == 9)
        {
            compteurligne = 0;
        }

        

        changement_regle(newr1, compteurcolone, matrice_alea[compteurcolone][compteurligne]);
        changement_regle(newr2, compteurcolone, matrice_alea[compteurcolone][compteurligne]);
        changement_regle(newr3, compteurcolone, matrice_alea[compteurcolone][compteurligne]);
        compteurcolone += 1;

        printf("%f\n", moyenne);
    }
    free(newr1);
    free(newr2);
    free(newr3);
    return r1;
}
