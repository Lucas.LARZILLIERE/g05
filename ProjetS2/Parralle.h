#include "threads.h"
#include <stdio.h>
#include "regles.h"

typedef struct
{
    int **layerFixe, **layerDynamique;
    position *j1, *j2, *j3;
    int points[100][2];
    int pointsRestants;
    Regle *r1, *r2, *r3;
    int *pointsPoule, *pointsRenard, *pointsVipere;
    int spawnPoule[2], spawnRenard[2], spawnVipere[2];
    int posPoule[2], posRenard[2], posVipere[2];
} argumentThread;

int treatment(void *parameters);
int treatment2(void *parameters);
int thread();
