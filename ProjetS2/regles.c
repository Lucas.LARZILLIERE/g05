#include "regles.h"

Regle *initRegles()
{
    Regle *regles;
    regles = (Regle *)malloc(NB_REGLES * sizeof(Regle));

    int aleaJoker;
    int choix;
    for (int i = 0; i < NB_REGLES; i++)
    {
        // Cases autour
        aleaJoker = rand() % 10;
        if (aleaJoker < 8)
            choix = -1;
        else
            choix = rand() % 4 - 1;
        regles[i].devant = choix;
        aleaJoker = rand() % 10;
        if (aleaJoker < 8)
            choix = -1;
        else
            choix = rand() % 4 - 1;
        regles[i].derrière = choix;
        aleaJoker = rand() % 10;
        if (aleaJoker < 8)
            choix = -1;
        else
            choix = rand() % 4 - 1;
        regles[i].gauche = choix;
        aleaJoker = rand() % 10;
        if (aleaJoker < 8)
            choix = -1;
        else
            choix = rand() % 4 - 1;
        regles[i].droite = choix;

        // Positions joueurs
        /*
        aleaJoker = rand() % 10;
        if (aleaJoker  < 8)
            choix = -1;
        else
            choix = rand() % 5 - 1;
        regles[i].directionJoueur1 = rand() % 5 - 1;
        aleaJoker = rand() % 10;
        if (aleaJoker  < 8)
            choix = -1;
        else
            choix = rand() % 5 - 1;
        regles[i].directionJoueur2 = rand() % 5 - 1;
        */
        regles[i].directionJoueur1 = -1;
        regles[i].directionJoueur2 = -1;

        // Priorité
        regles[i].priorite = rand() % 10 + 1;

        // Action
        regles[i].action = rand() % 4;
    }
    /*
    regles[0] = (Regle){-1, -1, -1, -1, -1, -1, -1, 5, HAUT};
    regles[1] = (Regle){-1, -1, -1, -1, -1, -1, -1, 5, BAS};
    regles[2] = (Regle){-1, -1, -1, -1, -1, -1, -1, 5, GAUCHE};
    regles[3] = (Regle){-1, -1, -1, -1, -1, -1, -1, 5, DROITE};
    regles[4] = (Regle){-1, -1, -1, -1, -1, -1, -1, 5, HAUT};
    regles[5] = (Regle){-1, -1, -1, -1, -1, -1, -1, 5, BAS};
    regles[6] = (Regle){-1, -1, -1, -1, -1, -1, -1, 5, GAUCHE};
    regles[7] = (Regle){-1, -1, -1, -1, -1, -1, -1, 5, DROITE};
    regles[8] = (Regle){-1, -1, -1, -1, -1, -1, -1, 5, HAUT};
    regles[9] = (Regle){-1, -1, -1, -1, -1, -1, -1, 5, BAS};
    regles[10] = (Regle){0, 0, 0, 0, 0, 0, 0, 0, BAS};
    regles[11] = (Regle){0, 0, 0, 0, 0, 0, 0, 0, BAS};
    regles[12] = (Regle){0, 0, 0, 0, 0, 0, 0, 0, BAS};
    regles[13] = (Regle){0, 0, 0, 0, 0, 0, 0, 0, BAS};
    regles[14] = (Regle){0, 0, 0, 0, 0, 0, 0, 0, BAS};
    regles[15] = (Regle){0, 0, 0, 0, 0, 0, 0, 0, BAS};
    regles[16] = (Regle){0, 0, 0, 0, 0, 0, 0, 0, BAS};
    regles[17] = (Regle){0, 0, 0, 0, 0, 0, 0, 0, BAS};
    regles[18] = (Regle){0, 0, 0, 0, 0, 0, 0, 0, BAS};
    regles[NB_REGLES - 1] = (Regle){0, 0, 0, 0, 0, 0, 0, 0, BAS};*/
    return regles;
}

// Retourne la direction d'où est b par rapport à a
int GetDirection(position a, position b)
{
    int differenceY = a.y - b.y;
    int differenceX = a.x - b.x;

    if (abs(differenceX) > abs(differenceY)) // Si le joueur est plus proche sur les X que les Y
    {
        if (differenceX < 0)
        {
            return DROITE;
        }
        else
        {
            return GAUCHE;
        }
    }
    else
    {
        if (differenceY < 0)
        {
            return BAS;
        }
        else
        {
            return HAUT;
        }
    }
}

// Retourne la proximité entre le point a et le point b : PROCHE ou LOIN
int GetProximite(position a, position b)
{
    int differenceY = a.y - b.y;
    int differenceX = a.x - b.x;

    if (differenceX < 25 && differenceY < 25)
    {
        return 0; // Proche
    }
    else
    {
        return 1;
    }
}

// Retourne les cases autour de a
int *GetCasesAutour(int **jeu, position a)
{
    int *cases;
    cases = (int *)malloc(4 * sizeof(int));

    cases[0] = jeu[a.y - 1][a.x]; // Case du haut
    cases[1] = jeu[a.y + 1][a.x]; // Case du bas
    cases[2] = jeu[a.y][a.x - 1]; // Case de gauche
    cases[3] = jeu[a.y][a.x + 1]; // Case de droite

    return cases;
}

// Retourne le point le plus proche du joueur a
int GetPointLePlusProche(int points[100][2], position a, int pointsRestants)
{
    int proche = 50000;
    int plusProche[2];
    plusProche[0] = points[0][0];
    plusProche[1] = points[0][1];
    int longueurX;
    int longueurY;
    int longMax;
    for (int i = 0; i < pointsRestants; i++)
    {
        longueurX = abs(a.x - points[i][0]);
        longueurY = abs(a.y - points[i][1]);
        if (longueurX < longueurY)
            longMax = longueurY;
        else
            longMax = longueurX;
        if (longMax < proche)
        {
            plusProche[0] = points[i][0];
            plusProche[1] = points[i][1];
            proche = longMax;
        }
    }

    int retour;
    if (abs(plusProche[0] - a.x) > abs(plusProche[1] - a.y)) // On retourne un y
    {
        if (plusProche[1] - a.y > 0)
            retour = S;
        else
            retour = N;
    }
    else
    {
        if (plusProche[0] - a.x > 0)
            retour = E;
        else
            retour = O;
    }

    return retour;
}

// Permet de filtrer les règles pour ne garder que les règles qui nous intéressent dans le cas présent
Regle *FiltrageRegles(Regle *regles, int *cases, int directionJoueur1, int directionJoueur2, int *nbRegles)
{
    int indice = 0;

    Regle regleTempo;
    if (cases[0] == 1 || cases[0] == 7 || cases[0] == 8 || cases[0] == 9)
        regleTempo.devant = OBSTACLE;
    if (cases[1] == 1 || cases[1] == 7 || cases[1] == 8 || cases[1] == 9)
        regleTempo.derrière = OBSTACLE;
    if (cases[2] == 1 || cases[2] == 7 || cases[2] == 8 || cases[2] == 9)
        regleTempo.gauche = OBSTACLE;
    if (cases[3] == 1 || cases[3] == 7 || cases[3] == 8 || cases[3] == 9)
        regleTempo.droite = OBSTACLE;
    regleTempo.directionJoueur1 = directionJoueur1;
    regleTempo.directionJoueur2 = directionJoueur2;

    for (int i = 0; i < NB_REGLES; i++)
    {
        if (regles[i].devant == -1 || regleTempo.devant == regles[i].devant)
        {
            if (regles[i].derrière == -1 || regleTempo.derrière == regles[i].derrière)
            {
                if (regles[i].gauche == -1 || regleTempo.gauche == regles[i].gauche)
                {
                    if (regles[i].droite == -1 || regleTempo.droite == regles[i].droite)
                    {
                        if (regles[i].directionJoueur1 == -1 || regleTempo.directionJoueur1 == regles[i].directionJoueur1)
                        {
                                if (regles[i].directionJoueur2 == -1 || regleTempo.directionJoueur2 == regles[i].directionJoueur2)
                                {

                                    indice += 1;
                                }
                        }
                    }
                }
            }
        }
    }

    Regle *reglesFiltrees;
    reglesFiltrees = (Regle *)malloc((indice) * sizeof(Regle));

    indice = 0;
    for (int i = 0; i < NB_REGLES; i++)
    {
        if (regles[i].devant == -1 || regleTempo.devant == regles[i].devant)
        {
            if (regles[i].derrière == -1 || regleTempo.derrière == regles[i].derrière)
            {
                if (regles[i].gauche == -1 || regleTempo.gauche == regles[i].gauche)
                {
                    if (regles[i].droite == -1 || regleTempo.droite == regles[i].droite)
                    {
                        if (regles[i].directionJoueur1 == -1 || regleTempo.directionJoueur1 == regles[i].directionJoueur1)
                        {
                                if (regles[i].directionJoueur2 == -1 || regleTempo.directionJoueur2 == regles[i].directionJoueur2)
                                {
                                    reglesFiltrees[indice] = regles[i];
                                    indice += 1;
                                }
                        }
                    }
                }
            }
        }
    }

    *nbRegles = indice;
    return reglesFiltrees;
}

Regle choix_regle(Regle *LesRegles, int nbRegles)
{
    int SomePrio = 0;
    for (int i = 0; i < nbRegles; i++)
    {
        SomePrio += LesRegles[i].priorite;
    }
    int alea = rand() % SomePrio;
    int passage = 0;
    int i = 0;
    while (LesRegles[i].priorite + passage < alea)
    {
        passage += LesRegles[i].priorite;
        i += 1;
    }

    return LesRegles[i];
}