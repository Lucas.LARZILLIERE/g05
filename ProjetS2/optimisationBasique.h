#include "jeu.h"
#include "convertMatriceToSDL2.h"
#include <time.h>

void afficherRègles(Regle *r1, Regle *r2, Regle *r3);

void partie(int **layerFixe, int **layerDynamique, position *j1, position *j2, position *j3,
            Regle *r1, Regle *r2, Regle *r3,
            int spawnPoule[2], int spawnRenard[2], int spawnVipere[2],
            int *bestPoints,
            int posPoule[2], int posRenard[2], int posVipere[2]
            );

float moyenneParties(int nbParties, int **layerFixe, int **layerDynamique, position *j1, position *j2, position *j3,
                   Regle *r1, Regle *r2, Regle *r3,
                   int spawnPoule[2], int spawnRenard[2], int spawnVipere[2],
                   int *bestPoints,
                   int posPoule[2], int posRenard[2], int posVipere[2]);


Regle *  trouver_le_meilleur_cerveau(int nombre_partie, int nombre_iteration,int **layerFixe, int **layerDynamique, position *j1, position *j2, position *j3, Regle *r1, Regle *r2, Regle *r3, int spawnPoule[2], int spawnRenard[2], int spawnVipere[2]);
