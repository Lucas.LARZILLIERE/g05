#include "utilMatrice.h"




void affichageMatrice(int ** matrice, int taille)
{
    for (int i = 0; i < taille; i++)
    {
        printf("\n");
        for (int j = 0; j < taille; j++)
        {
            printf(" %d ", matrice[i][j]);
        }
    }
    printf("\n");
}



int ** initMatrice(int taille)
{
    // Init matrice :

    int ** matrice = (int ** )malloc(taille * sizeof(int *));
    for (int i = 0; i < taille; i++)
    {
        matrice[i] = (int *)malloc(taille * sizeof(int));
        for (int j = 0; j < taille; j++)
        {
            matrice[i][j] = 0;
        }
    }

    return matrice;
}


void freeMatrice(int ** matrice, int taille)
{
    for (int i = 0; i < taille; i++)
    {
        free(matrice[i]); // Libérer chaque ligne de la matrice
    }

    free(matrice); // Libérer le tableau de pointeurs vers les lignes
    matrice = NULL;

}



