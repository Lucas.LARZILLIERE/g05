#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>

// Permet d'initialiser une fenêtre
SDL_Window * initWindow(int x, int y, int w, int h, char * name);

// Permet d'initialiser un Renderer
SDL_Renderer * initRenderer(SDL_Window * window);

// Arrêter tout le rendu en SDL, en vérifiant ce qui ne va pas
void end_sdl(char ok,  char const *msg, SDL_Window *window, SDL_Renderer *renderer);

// Permet d'initialiser un texte en passant en paramètre la fenêtre, le renderer, le texte et la position où le mettre
SDL_Texture * initText(SDL_Window * window, SDL_Renderer * renderer, char * text, SDL_Rect pos);

// Permet de charger une texture à partir d'une image en passant en paramètre le lien, la fenêtre et le renderer
SDL_Texture* load_texture_from_image(char  *  file_image_name, SDL_Window *window, SDL_Renderer *renderer );

// Initialisation et remplissage ( à partir des chemins dans 'images') d'un tableau de texture
SDL_Texture** create_tabTexture(SDL_Window* window, SDL_Renderer* renderer,char ** images);

// Détruit le tableau de texture
void destroy_tabTexture(SDL_Texture** tabAnimSpriteTexture);

// Initialise une tableau de rectangle et met à zéro tous ces composants
SDL_Rect * init_tabSDL_Rect(int taille);

// Initialise une tableau de SDL_bool et les mets à FAUX
SDL_bool * init_tabSDL_bool(int taille);

// Met à FAUX le tableau de SDL_bool
SDL_bool * reset_tabSDL_bool(SDL_bool * tab, int taille);

// Met à jour le tableau de booléen tabSourisSurBouton
SDL_bool * update_tabSourisSurBouton(SDL_bool * tabEtat,SDL_bool * tabSourisSurBouton,SDL_Rect * tabBoutonRect,int mouseX,int mouseY);