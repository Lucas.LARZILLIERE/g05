#include "convertMatriceToSDL2.h"

int** convert_MatriceStatic(int** matrice, int taille) {
    int** newMatrice = initMatrice(taille);
    int a;

    for (int i = 0; i < taille; i++) {
        for (int j = 0; j < taille; j++) {
            switch (matrice[i][j]) {
                case 0:
                    a = 0;
                    if (rand() % 3 != 2)
                        a = 1;
                    newMatrice[i][j] = a;
                    break;
                case 1:
                    newMatrice[i][j] = 2;
                    break;
                case 2:
                    newMatrice[i][j] = 3;
                    break;
                case 7:
                    newMatrice[i][j] = 4;
                    break;
                case 8:
                    newMatrice[i][j] = 5;
                    break;
                case 9:
                    newMatrice[i][j] = 6;
                    break;
                default:
                    break;
            }
        }
    }
    return newMatrice;
}

int** convert_MatricePlayer(int** matrice, int taille) {
    int** newMatrice = initMatrice(taille);

    for (int i = 0; i < taille; i++) {
        for (int j = 0; j < taille; j++) {
            switch (matrice[i][j]) {
                case 71:
                    newMatrice[i][j] = 0;
                    break;
                case 81:
                    newMatrice[i][j] = 1;
                    break;
                case 91:
                    newMatrice[i][j] = 2;
                    break;
                default:
                    newMatrice[i][j] = -1;
                    break;
            }
        }
    }
    return newMatrice;
}

int** create_MatriceBG(int taille) {
    int** newMatrice = initMatrice(taille);
    int a;

    for (int i = 0; i < taille; i++) {
        for (int j = 0; j < taille; j++) {
            a = 0;
            if (rand() % 3 != 2)
                a = 1;
            newMatrice[i][j] = a;

        }
    }
    return newMatrice;
}

char** allouerTableauChaine(int taille) {
    // Allouer dynamiquement le tableau de pointeurs
    char** tableau = malloc(taille * sizeof(char*));

    // Allocation de mémoire pour chaque élément du tableau
    for (int i = 0; i < taille; i++) {
        tableau[i] = malloc(10 * sizeof(char)); // Allocation de 10 caractères pour chaque chaîne
        sprintf(tableau[i], "%d", 0); // Initialiser chaque chaîne à "0"
    }

    return tableau;
}

void intToChar_TabScore(char ** tableau, int pointsPoule, int pointsRenard, int pointsVipere) {
    sprintf(tableau[0], "%d", pointsPoule);
    sprintf(tableau[1], "%d", pointsRenard);
    sprintf(tableau[2], "%d", pointsVipere);
}

void libererTableauChaine(char** tableau, int taille) {
    for (int i = 0; i < taille; i++) {
        free(tableau[i]);
    }
    free(tableau);
}
