#include "matrice_jeu.h"
#include <time.h>

int **creation_du_monde(int **monde, int *nb_tresor, int taille, int points[100][2], int spawnPoule[2], int spawnRenard[2], int spawnVipere[2])
{

    for (int i = 0; i < taille; i++)
    {
        monde[0][i] = 1;
        monde[taille - 1][i] = 1;
        monde[i][0] = 1;
        monde[i][taille - 1] = 1;
    }

    int cabane_poule = rand() % taille / 3 + 1;
    int cabane_vipere = rand() % (taille / 3) + 2 * taille / 3 - 1;
    int cabane_ranard = rand() % (taille / 3) + taille / 3; // placement de la base entre 1/3 et 2/3 des bords

    // Poule
    monde[0][cabane_poule] = 7;
    spawnPoule[0] = cabane_poule;
    spawnPoule[1] = 1;

    // Vipere
    monde[cabane_vipere][0] = 9;
    spawnVipere[0] = 1;
    spawnVipere[1] = cabane_vipere;

    // Renard
    monde[cabane_ranard][taille - 1] = 8;
    spawnRenard[0] = taille - 2;
    spawnRenard[1] = cabane_ranard;

    // injection obstacle

    for (int i = 0; i < 500; i++)
    {
        int obj = rand() % 4;
        int x = rand() % taille;
        int y = rand() % taille;

        if (obj == 0 && x > 1 && y > 1 && x < taille - 2 && y < taille - 2) // bloc 1*1
        {
            if (monde[x + 1][y] != 1 && monde[x][y + 1] != 1 && monde[x - 1][y] != 1 && monde[x][y - 1] != 1 && monde[x - 1][y - 1] != 1 && monde[x - 1][y + 1] != 1 && monde[x + 1][y - 1] != 1 && monde[x + 1][y + 1] != 1)
            {
                monde[x][y] = 1;
            }
        }
        if (obj == 1 && x > 2 && y > 2 && x < taille - 3 && y < taille - 3) // bloc 2*2

        {
            if (monde[x][y + 1] != 1 && monde[x + 1][y + 1] != 1 && monde[x + 2][y + 1] != 1 && monde[x - 1][y + 1] != 1 && monde[x][y - 2] != 1 && monde[x + 1][y - 2] != 1 && monde[x + 2][y - 2] != 1 && monde[x - 1][y - 2] != 1 && monde[x - 1][y] != 1 && monde[x - 1][y - 1] != 1 && monde[x + 2][y] != 1 && monde[x + 2][y - 1] != 1)
            {
                monde[x][y] = 1;
                monde[x + 1][y] = 1;
                monde[x][y - 1] = 1;
                monde[x + 1][y - 1] = 1;
            }
        }
        if (obj == 2 && x > 1 && y > 1 && x < taille - 3 && y < taille - 2) // bloc 2*1 2 blocs en colo
        {
            if (monde[x][y + 1] != 1 && monde[x + 1][y + 1] != 1 && monde[x + 2][y + 1] != 1 && monde[x - 1][y + 1] != 1 && monde[x][y - 1] != 1 && monde[x + 1][y - 1] != 1 && monde[x + 2][y - 1] != 1 && monde[x - 1][y - 1] != 1 && monde[x - 1][y] != 1 && monde[x + 2][y] != 1)
            {
                monde[x][y] = 1;
                monde[x + 1][y] = 1;
            }
        }
        if (obj == 3 && x > 1 && y > 1 && x < taille - 2 && y < taille - 3) // bloc 1*2 bloc en ligne
        {
            if (monde[x + 1][y] != 1 && monde[x + 1][y + 1] != 1 && monde[x + 1][y + 2] != 1 && monde[x + 1][y - 1] != 1 && monde[x - 1][y] != 1 && monde[x - 1][y - 1] != 1 && monde[x - 1][y - 1] != 1 && monde[x - 1][y + 2] != 1 && monde[x][y - 1] != 1 && monde[x][y + 2] != 1)
            {
                monde[x][y] = 1;
                monde[x][y + 1] = 1;
            }
        }
    }

    for (int i = 1; i < taille - 2; i++)
    {
        for (int j = 1; j < taille - 2; j++)
        {
            int tresor = rand() % 20;
            if (monde[i][j] != 1 && tresor == 1  )
            {
                monde[i][j] = 2;
                points[*nb_tresor][0] = i;
                points[*nb_tresor][1] = j;
                *nb_tresor += 1;
            }
        }
    }

    if ( monde[1][cabane_poule] == 2 )
    {
        monde[1][cabane_poule] = 0;
        *nb_tresor -= 1;

    }
    if ( monde[cabane_vipere][1] == 2 )
    {
        monde[cabane_vipere][1] = 0;
        *nb_tresor -= 1;

    }
     
    if (monde[cabane_ranard][taille - 2] == 2)
    {
        monde[cabane_ranard][taille - 2]  = 0;
        *nb_tresor -= 1;

    }
    
    return monde;
}

int **placement_joueur(int **monde_dep, int **monde, int taille, int poule[2], int renard[2], int vipere[2])
{
    for (int i = 0; i < taille; i++)
    {
        for (int j = 0; j < taille; j++)
        {
            monde_dep[i][j] = monde[i][j];
        }
    }
    for (int i = 0; i < taille; i++)
    {
        if (monde_dep[0][i] == 7)
        {
            monde_dep[1][i] = 71; // poule1
            poule[1] = 1;
            poule[0] = i;
        }
    }
    for (int i = 0; i < taille; i++)
    {
        if (monde_dep[i][taille - 1] == 8)
        {
            monde_dep[i][taille - 2] = 81; // renard1
            renard[1] = i;
            renard[0] = taille - 2;
        }
        if (monde_dep[i][0] == 9)
        {
            monde_dep[i][1] = 91; // vipere1
            vipere[1] = i;
            vipere[0] = 1;
        }
    }
    return monde_dep;
}
