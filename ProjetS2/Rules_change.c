#include "Rules_change.h"
#include <time.h>

void changement_regle(Regle *Regles,int num_regles, int parametre_a_changer )
{
   
    
    switch (parametre_a_changer)
    {

    case 0:

        
        Regles[num_regles].devant +=1 ;
        if (Regles[num_regles].devant==3)
        {
            Regles[num_regles].devant=-1;
        }
        
        break;

    case 1:
      
        Regles[num_regles].derrière +=1 ;
        if (Regles[num_regles].derrière==3)
        {
            Regles[num_regles].derrière=-1;
        }
        

        break;
    case 2:

        Regles[num_regles].gauche +=1 ;
        if (Regles[num_regles].gauche==3)
        {
            Regles[num_regles].gauche=-1;
        }
       


        break;

    case 3:

        Regles[num_regles].droite +=1 ;
        if (Regles[num_regles].droite==3)
        {
            Regles[num_regles].droite=-1;
        }
        
        break;

    case 4:


        Regles[num_regles].directionJoueur1 +=1 ;
        if (Regles[num_regles].directionJoueur1==4)
        {
            Regles[num_regles].directionJoueur1=-1;
        }
        

        break;

    case 5:
        Regles[num_regles].directionJoueur2 +=1 ;
        if (Regles[num_regles].directionJoueur2==4)
        {
            Regles[num_regles].directionJoueur2=-1;
        }
       

        break;

    case 6:

        Regles[num_regles].priorite +=1 ;
        if (Regles[num_regles].priorite==11)
        {
            Regles[num_regles].priorite=1;
        }
        

        

        break;

    case 7:

        Regles[num_regles].action +=1 ;
        if (Regles[num_regles].action==4)
        {
            Regles[num_regles].action=0;
        }
        


        break;
    }
}







int ** matrice_changements(int ** matrice )
{
    
    for (int i = 0; i < NB_REGLES ; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            matrice[i][j]=j; // cree une liste de 0 a 7 partout
        }
        
    }
    
    for (int i = 0; i < NB_REGLES; i++)
    {
         for (int k = 7 ; k >= 0; k--)
          {
            int j = rand() %8; //on inverse des positions aleatoirement
            int temp = matrice[i][j];
            matrice[i][j] = matrice[i][k];
            matrice[i][k] = temp;
        }
    }
    
    return matrice;
}
