#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <limits.h>
#include "utilMatrice.h"
#include <string.h>

int ** creation_du_monde(int ** monde , int * nb_tresor, int taille, int points[100][2], int spawnPoule[2], int spawnRenard[2], int spawnVipere[2]);
int ** placement_joueur( int ** monde_dep,int ** monde, int taille, int poule[2], int renard[2], int vipere[2]);
