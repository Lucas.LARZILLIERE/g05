#include "SDL2.h"

SDL_Rect* create_tabStaticSpriteSource() {
    SDL_Rect* tab = init_tabSDL_Rect(9);  // Allocation du tableau

    for (int i = 0; i < 9; i++) {
        tab[i].w = DIM_SPRITE_STATIC;  // Définition de la taille des sprites
        tab[i].h = DIM_SPRITE_STATIC;
    }

    tab[0].x = 85;  // Définition des positions des textures
    tab[0].y = 0;   // Voir SDL2.h pour les détails
    tab[1].x = 85;
    tab[1].y = 17;
    tab[2].x = 255;
    tab[2].y = 154;
    tab[3].x = 731;
    tab[3].y = 170;
    tab[4].x = 663;
    tab[4].y = 17;
    tab[6].x = 918;
    tab[6].y = 357;
    tab[5].x = 408;
    tab[5].y = 187;
    tab[7].x = 51;
    tab[7].y = 255;
    tab[8].x = 51;
    tab[8].y = 272;
    return tab;
}

SDL_Rect* create_tabGameIconSpriteSource() {
    SDL_Rect* tab = init_tabSDL_Rect(2);  // Allocation du tableau

    for (int i = 0; i < 2; i++) {
        tab[i].w = DIM_SPRITE_ICON;  // Définition de la taille des sprites
        tab[i].h = DIM_SPRITE_ICON;
    }

    tab[0].x = 400;  // Définition des positions des textures
    tab[0].y = 50;   // Voir SDL2.h pour les détails
    return tab;
}

void draw_background(SDL_Texture *my_texture,SDL_Rect * tabStaticSpriteSource, SDL_Renderer *renderer,int windowDim,int ** matrice,int taille) {
    SDL_Rect destination = { 0 };  // Rectangle définissant où la zone_source doit être déposée dans le renderer

    double zoom = (double)windowDim / (DIM_SPRITE_STATIC * (double)taille);

    destination.x = 0;
    destination.y = 0;
    destination.w = DIM_SPRITE_STATIC * zoom;
    destination.h = DIM_SPRITE_STATIC * zoom;

    for (int i = 0; i < taille; i++) {
        for (int j = 0; j < taille; j++) {
            SDL_RenderCopy(renderer, my_texture, &tabStaticSpriteSource[matrice[i][j]], &destination);  // Création de l'herbe_1
            destination.x += DIM_SPRITE_STATIC* zoom;
        }
        destination.y += DIM_SPRITE_STATIC * zoom;
        destination.x = 0.0;
    }
}

void draw_static(SDL_Texture* my_texture, SDL_Rect* tabStaticSpriteSource, SDL_Renderer* renderer, int windowDim, int** matrice, int taille) {
    SDL_Rect destination = { 0 };  // Rectangle définissant où la zone_source doit être déposée dans le renderer

    double zoom = (double)windowDim / (DIM_SPRITE_STATIC * (double)taille);

    destination.x = 0;
    destination.y = 0;
    destination.w = DIM_SPRITE_STATIC * zoom;
    destination.h = DIM_SPRITE_STATIC * zoom;

    for (int i = 0; i < taille; i++) {
        for (int j = 0; j < taille; j++) {
            if (matrice[i][j]!=0 && matrice[i][j]!=1 )   
                SDL_RenderCopy(renderer, my_texture, &tabStaticSpriteSource[matrice[i][j]], &destination);  // Création de l'herbe_1
            destination.x += DIM_SPRITE_STATIC * zoom;
        }
        destination.y += DIM_SPRITE_STATIC  * zoom;
        destination.x = 0.0;
    }
}

void draw_player(SDL_Renderer* renderer, SDL_Texture** tabAnimSpriteTexture, int** matrice, int taille, int windowDim, int etape) {
    SDL_Rect source = { 0 };
    SDL_Rect destination = { 0 };

    double zoom = (double)windowDim / (DIM_SPRITE_ANIM * (double)taille);

    source.x = DIM_SPRITE_ANIM * etape;
    source.y = 0;
    source.w = DIM_SPRITE_ANIM;
    source.h = DIM_SPRITE_ANIM;

    for (int i = 0; i < taille; i++) {
        for (int j = 0; j < taille; j++) {
            if (matrice[i][j] != -1) {
                destination.x = j * DIM_SPRITE_ANIM * zoom;
                destination.y = i * DIM_SPRITE_ANIM * zoom;
                destination.w = DIM_SPRITE_ANIM * zoom;
                destination.h = DIM_SPRITE_ANIM * zoom;
                SDL_RenderCopy(renderer, tabAnimSpriteTexture[matrice[i][j]+2], &source, &destination);
            }
        }
    }
}

void draw_menuPause(SDL_Renderer * renderer, SDL_Texture * menuPause, int taille , int windowDim){
    SDL_Rect    source = {0},
                destination = { 0 };  // Rectangle définissant où la zone_source doit être déposée dans le renderer

    double zoom = (double)windowDim / (DIM_SPRITE_ANIM * (double)taille);
    double tailleJeu = DIM_SPRITE_ANIM*zoom*(double)taille;

    SDL_QueryTexture(menuPause, NULL, NULL,
    &source.w, &source.h);       // Récupération des dimensions de l'image

    destination.w = tailleJeu *0.6;
    destination.h = tailleJeu*0.6;
    destination.x = (tailleJeu-destination.h)/2;
    destination.y = (tailleJeu-destination.h)/2;

    SDL_RenderCopy(renderer, menuPause,
        &source,
        &destination);                 // Création de l'élément à afficher
}

void draw_scores(SDL_Renderer* renderer, SDL_Texture** tabAnimSpriteTexture,int tabScoreDim, int windowDim) {
    SDL_Rect    source = { 0 },
                destination = { 0 };

    float zoom = (float)tabScoreDim*0.8/DIM_SPRITE_ANIM;

    source.x = DIM_SPRITE_ANIM;
    source.y = 0;
    source.w = DIM_SPRITE_ANIM;
    source.h = DIM_SPRITE_ANIM;
    destination.x = windowDim+tabScoreDim*0.1;
    destination.w = DIM_SPRITE_ANIM * zoom;
    destination.h = DIM_SPRITE_ANIM * zoom;

    for (int j = 0; j < 3; j++) {
        destination.y = j * DIM_SPRITE_ANIM * zoom*2;
        SDL_RenderCopy(renderer, tabAnimSpriteTexture[j+2], &source, &destination);
    }
}

void write_scores(SDL_Renderer* renderer, int tabScoreDim, int windowDim, char ** score ) {
    TTF_Font* font = NULL;
    SDL_Rect destination = {0, 0, 0, 0}; 
    SDL_Surface* text_surface = NULL;
    SDL_Texture* text_texture = NULL;
    SDL_Color color = {255, 255, 255, 255};                                                  // la variable 'police de caractère'
    float zoom = (float)tabScoreDim*0.8/DIM_SPRITE_ANIM;

    font = TTF_OpenFont("./sprite/VCR_OSD_MONO_1.001.ttf", DIM_SPRITE_STATIC*zoom);                     // La police à charger, la taille désirée

    destination.y = (DIM_SPRITE_ANIM * zoom);

    for (int j = 0; j < 3; j++) {

        text_surface = TTF_RenderText_Blended(font, score[j] , color); // création du texte dans la surface 
        text_texture = SDL_CreateTextureFromSurface(renderer, text_surface); // transfert de la surface à la texture
        SDL_QueryTexture(text_texture, NULL, NULL, &destination.w, &destination.h);          // récupération de la taille (w, h) du texte 
        destination.x = windowDim+tabScoreDim/2-destination.w/2;
        SDL_RenderCopy(renderer, text_texture, NULL, &destination);
        destination.y +=DIM_SPRITE_ANIM * zoom*2;
    }
    SDL_FreeSurface(text_surface);
    SDL_DestroyTexture(text_texture);
}

SDL_Rect draw_jeuBoutons(SDL_Renderer * renderer, SDL_Texture * gameIconsTexture,SDL_Rect* tabGameIconSpriteSource,SDL_bool gameBoutonMenuBG, int tabScoreDim , int windowDim){
    SDL_Rect    destination = { 0 };  // Rectangle définissant où la zone_source doit être déposée dans le renderer

    float zoom = (float)tabScoreDim*0.8/DIM_SPRITE_ICON;

    destination.w = DIM_SPRITE_ICON*zoom;
    destination.h = DIM_SPRITE_ICON*zoom;
    destination.x = windowDim+tabScoreDim/2-destination.w/2;
    destination.y = windowDim-destination.w;

    if (gameBoutonMenuBG) {
        SDL_SetRenderDrawColor(renderer,68, 187, 182,255);
        SDL_RenderFillRect(renderer, &destination);
    }
    else {
        SDL_SetRenderDrawColor(renderer,55, 170, 160,255);
        SDL_RenderFillRect(renderer, &destination);
    }

    SDL_RenderCopy(renderer, gameIconsTexture,
        &tabGameIconSpriteSource[0],
        &destination);                 // Création de l'élément à afficher
    return destination;
}

void write_menuSubTitle(SDL_Renderer* renderer,int windowDim,int tabScoreDim) {
    TTF_Font* font = NULL;
    SDL_Rect destination = {0, 0, 0, 0}; 
    SDL_Surface* text_surface = NULL;
    SDL_Texture* text_texture = NULL;
    SDL_Color color = {255, 255, 255, 255};                                                  // la variable 'police de caractère'

    font = TTF_OpenFont("./sprite/VCR_OSD_MONO_1.001.ttf", 100);                     // La police à charger, la taille désirée

    text_surface = TTF_RenderText_Blended(font, "Qui fonctionne relativement bien ..." , color); // création du texte dans la surface 
    text_texture = SDL_CreateTextureFromSurface(renderer, text_surface); // transfert de la surface à la texture
    SDL_QueryTexture(text_texture, NULL, NULL, &destination.w, &destination.h);          // récupération de la taille (w, h) du texte 

    float zoom = (float)(windowDim+tabScoreDim)*0.6/(float)destination.w;
    
    destination.x = (windowDim+tabScoreDim)/2-destination.w*zoom/2;
    destination.y = windowDim*0.3-destination.h*zoom/2;
    destination.w*=zoom;
    destination.h*=zoom;

    SDL_RenderCopy(renderer, text_texture, NULL, &destination);

    SDL_FreeSurface(text_surface);
    SDL_DestroyTexture(text_texture);
}

void draw_menuHerbe(SDL_Texture *my_texture,SDL_Rect * tabStaticSpriteSource, SDL_Renderer *renderer,int windowDim,int tabScoreDim) {
    SDL_Rect destination = { 0 };  // Rectangle définissant où la zone_source doit être déposée dans le renderer

    double zoom = (double)(windowDim+tabScoreDim) / (DIM_SPRITE_STATIC * 20.);

    destination.x = 0;
    destination.y = DIM_SPRITE_STATIC * zoom*6;
    destination.w = DIM_SPRITE_STATIC * zoom;
    destination.h = DIM_SPRITE_STATIC * zoom;

    for (int i = 0; i < 14; i++) {
        for (int j = 0; j < 20; j++) {
            if (i==0) SDL_RenderCopy(renderer, my_texture, &tabStaticSpriteSource[7], &destination);
            else SDL_RenderCopy(renderer, my_texture, &tabStaticSpriteSource[8], &destination);  // Création de l'herbe_1
            destination.x += DIM_SPRITE_STATIC* zoom;
        }
        destination.y += DIM_SPRITE_STATIC * zoom;
        destination.x = 0.0;
    }
}

float draw_menuTitreAnim(SDL_Texture *my_texture, SDL_Renderer *renderer,int windowDim,int tabScoreDim, float coef,int statut) {
    SDL_Rect    source = {0 ,0,0,0},
                destination = { 0 };  // Rectangle définissant où la zone_source doit être déposée dans le renderer

    SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h);  
    
    if (statut==0) coef+=0.01;
    if (statut==1) coef-=0.01;
    float zoom = (float)(windowDim+tabScoreDim)*coef/(float)source.w;
  
    destination.x = (windowDim+tabScoreDim)/2-source.w*zoom/2;
    destination.y = windowDim*0.2-source.h*zoom/2;
    destination.w = source.w * zoom;
    destination.h = source.h * zoom;

    SDL_RenderCopy(renderer, my_texture,
        &source,
        &destination); 

    return coef;
}

void draw_cloudAnim(SDL_Texture *my_texture, SDL_Renderer *renderer,int etapeMenu ,int windowDim,int tabScoreDim,int randNumber[3]) {
    SDL_Rect    source1 ,
                destination1 ,
                source2 ,
                destination2,
                source3 ,
                destination3 ;

    SDL_QueryTexture(my_texture, NULL, NULL, &source1.w, &source1.h);  
    float zoom = (float)(windowDim+tabScoreDim)*0.2/(float)source1.w;

    source1.x=0;
    source2.x=0;
    source3.x=0;
    source1.y=0;
    source2.y=50;
    source3.y=85;
    source2.w=source1.w;
    source3.w=source1.w;
    source1.h=50;
    source2.h=35;
    source3.h=42;

    destination1.w = source1.w * zoom;
    destination2.w = source2.w * zoom;
    destination3.w =source3.w * zoom;
    destination1.h = source1.h * zoom;
    destination2.h = source2.h * zoom;
    destination3.h = source3.h * zoom;

    destination1.x = randNumber[0]%(windowDim+tabScoreDim+1)+destination1.w+etapeMenu*1.5-1;
    destination2.x = randNumber[1]%(windowDim+tabScoreDim+1)+destination2.w-etapeMenu*2-101;
    destination3.x = randNumber[2]%(windowDim+tabScoreDim+1)+destination3.w+etapeMenu-201;

    destination1.y = windowDim*0.075-destination1.h/2;
    destination2.y = windowDim*0.15-destination2.h/2;
    destination3.y = windowDim*0.225-destination3.h/2;

    SDL_RenderCopy(renderer, my_texture,
        &source1,
        &destination1);
    SDL_RenderCopy(renderer, my_texture,
        &source2,
        &destination2); 
    SDL_RenderCopy(renderer, my_texture,
        &source3,
        &destination3);                 // Création de l'élément à afficher

    if(destination1.x+destination1.w >windowDim+tabScoreDim){
        destination1.x-=windowDim+tabScoreDim;
        SDL_RenderCopy(renderer, my_texture,&source1,&destination1); 
    }
    if(destination2.x < 0){
        destination2.x+=windowDim+tabScoreDim;
        SDL_RenderCopy(renderer, my_texture,&source2,&destination2); 
    }
    if(destination3.x+destination3.w >windowDim+tabScoreDim){
        destination3.x-=windowDim+tabScoreDim;
        SDL_RenderCopy(renderer, my_texture,&source3,&destination3); 
    }
}

SDL_Rect * draw_menuBoutons(SDL_Renderer * renderer,SDL_Texture ** tabTexture, SDL_bool * tabSourisSurBouton,SDL_Rect * tabBoutonRect, int windowDim,int tabScoreDim){
    SDL_Rect    source ={0,0,0,0},
                destination={0,0,0,0};
    
    SDL_QueryTexture(tabTexture[8], NULL, NULL, &source.w, &source.h);

    float zoom = (float)windowDim*0.6/(7*source.h);

    destination.w = source.w*zoom;
    destination.h = source.h*zoom;
    destination.x = (windowDim+tabScoreDim)/2-destination.w/2;
    destination.y = windowDim*0.4+destination.h;
    
    for (int i=0;i<3;i++) {            
        if (tabSourisSurBouton[i+1])
                SDL_RenderCopy(renderer, tabTexture[i*2+8],&source,&destination); 
        else SDL_RenderCopy(renderer, tabTexture[i*2+9],&source,&destination);
        tabBoutonRect[i+1]=destination;
        destination.y +=destination.h*2;
    }
    return tabBoutonRect;
}

void draw_credits(SDL_Texture *my_texture, SDL_Renderer *renderer,int windowDim,int tabScoreDim) {
    SDL_Rect    source = {0 ,0,0,0},
                destination = { 0 };  // Rectangle définissant où la zone_source doit être déposée dans le renderer

    SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h);  
    
    float zoom = (float)(windowDim+tabScoreDim)*0.9/(float)source.w;
  
    destination.x = (windowDim+tabScoreDim)/2-source.w*zoom/2;
    destination.y = windowDim/2-source.h*zoom/2;
    destination.w = source.w * zoom;
    destination.h = source.h * zoom;

    SDL_RenderCopy(renderer, my_texture,
        &source,
        &destination); 
}

void draw_menuPlayer(SDL_Renderer* renderer, SDL_Texture** tabAnimSpriteTexture, int windowDim,int tabScoreDim, int etape) {
    SDL_Rect source = { 0 };
    SDL_Rect destination = { 0 };
    int ** matrice =initMatrice(10);
    double zoom = (double)(windowDim+tabScoreDim) / (DIM_SPRITE_ANIM * 20.);

    srand(time(NULL));

    source.x = DIM_SPRITE_ANIM * (etape%4);
    source.y = 0;
    source.w = DIM_SPRITE_ANIM;
    source.h = DIM_SPRITE_ANIM;

    destination.w = DIM_SPRITE_ANIM * zoom*2;
    destination.h = DIM_SPRITE_ANIM * zoom*2;

    for (int k=0;k<3;k++){
        destination.x = 0;
        destination.y = DIM_SPRITE_ANIM *zoom*6;
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 10; j++) {
                if (((rand()%30)==10)&&(matrice[i][j]!=1)){
                    SDL_RenderCopy(renderer, tabAnimSpriteTexture[k+2], &source, &destination);
                    matrice[i][j]=1;
                }
                destination.x += DIM_SPRITE_ANIM* zoom*2;
            }
            destination.y += DIM_SPRITE_ANIM * zoom*2;
            destination.x = 0;
        }
    }
    freeMatrice(matrice,10);
}