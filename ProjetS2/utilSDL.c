#include "utilSDL.h"

SDL_Window * initWindow(int x, int y, int w, int h, char * name) {

    // Création de fenêtre :
    SDL_Window * window = NULL;
    window = SDL_CreateWindow(
        name, // codage en utf8, donc accents possibles
        x, y,  // coin haut gauche en haut gauche de l'écran
        w, h,  // dimensions de la fenêtre
        SDL_WINDOW_RESIZABLE);

    // Si erreur dans la création de la fenêtre :
    if (window == NULL)
    {
        SDL_Log("Error : SDL window creation - %s\n",
                SDL_GetError()); // échec de la création de la fenêtre
        SDL_Quit();              // On referme la SDL
        exit(EXIT_FAILURE);
    }

    return window;
}

SDL_Renderer * initRenderer(SDL_Window * window)
{
    SDL_Renderer* renderer = NULL;

    /* Création du renderer */
    renderer = SDL_CreateRenderer(window, -1,
                                  SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL)
        end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

    return renderer;
}

SDL_Texture* load_texture_from_image(char  *  file_image_name, SDL_Window *window, SDL_Renderer *renderer ){
    SDL_Surface *my_image = NULL;           // Variable de passage
    SDL_Texture* my_texture = NULL;         // La texture

    my_image = IMG_Load(file_image_name);   // Chargement de l'image dans la surface
                                            // image=SDL_LoadBMP(file_image_name); fonction standard de la SDL, 
                                            // uniquement possible si l'image est au format bmp */
    if (my_image == NULL) end_sdl(0, "Chargement de l'image impossible", window, renderer);
   
    my_texture = SDL_CreateTextureFromSurface(renderer, my_image); // Chargement de l'image de la surface vers la texture
    SDL_FreeSurface(my_image);                                     // la SDL_Surface ne sert que comme élément transitoire 
    if (my_texture == NULL) end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer);

    return my_texture;
}

void end_sdl(char ok,  char const *msg, SDL_Window *window, SDL_Renderer *renderer) { // renderer à fermer
    char msg_formated[255];
    int l;

    if (!ok)
    { // Affichage de ce qui ne va pas
        strncpy(msg_formated, msg, 250);
        l = strlen(msg_formated);
        strcpy(msg_formated + l, " : %s\n");

        SDL_Log(msg_formated, SDL_GetError());
    }

    if (renderer != NULL)
    {                                  // Destruction si nécessaire du renderer
        SDL_DestroyRenderer(renderer); // Attention : on suppose que les NULL sont maintenus !!
        renderer = NULL;
    }
    if (window != NULL)
    {                              // Destruction si nécessaire de la fenêtre
        SDL_DestroyWindow(window); // Attention : on suppose que les NULL sont maintenus !!
        window = NULL;
    }

    TTF_Quit();
    IMG_Quit();
    SDL_Quit();

    if (!ok)
    { // On quitte si cela ne va pas
        exit(EXIT_FAILURE);
    }
}

SDL_Texture * initText(SDL_Window * window, SDL_Renderer * renderer, char * text, SDL_Rect pos)
{
    if (TTF_Init() < 0) end_sdl(0, "Couldn't initialize SDL TTF", window, renderer);

  TTF_Font* font = NULL;                                               // la variable 'police de caractère'
  font = TTF_OpenFont("./font.ttf", 30);                     // La police à charger, la taille désirée
  if (font == NULL) end_sdl(0, "Can't load font", window, renderer);

  TTF_SetFontStyle(font, TTF_STYLE_ITALIC);           // en italique, gras

  SDL_Color color = {20, 0, 40, 255};                                  // la couleur du texte
  SDL_Surface* text_surface = NULL;                                    // la surface  (uniquement transitoire)
  text_surface = TTF_RenderText_Blended(font, text, color); // création du texte dans la surface 
  if (text_surface == NULL) end_sdl(0, "Can't create text surface", window, renderer);

  SDL_Texture* text_texture = NULL;                                    // la texture qui contient le texte
  text_texture = SDL_CreateTextureFromSurface(renderer, text_surface); // transfert de la surface à la texture
  if (text_texture == NULL) end_sdl(0, "Can't create texture from surface", window, renderer);
  SDL_FreeSurface(text_surface);                                       // la texture ne sert plus à rien

  SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h);          // récupération de la taille (w, h) du texte 
  SDL_RenderCopy(renderer, text_texture, NULL, &pos);                  // Ecriture du texte dans le renderer   
  SDL_DestroyTexture(text_texture);                                    // On n'a plus besoin de la texture avec le texte

  SDL_RenderPresent(renderer);                                         // Affichage 

  return text_texture;
}

SDL_Texture** create_tabTexture(SDL_Window* window, SDL_Renderer* renderer,char ** images) {
    SDL_Texture** tabAnimSpriteTexture = (SDL_Texture**)malloc((15) * sizeof(SDL_Texture*));

    for (int i=0;i<(15);i++) 
        tabAnimSpriteTexture[i] = load_texture_from_image(images[i], window, renderer);

    return tabAnimSpriteTexture;
}

void destroy_tabTexture(SDL_Texture** tabAnimSpriteTexture) {
    for (int i = 0; i < (15); i++) {
        if (tabAnimSpriteTexture[i] != NULL) {
            SDL_DestroyTexture(tabAnimSpriteTexture[i]);
        }
    }
    free(tabAnimSpriteTexture);
}

SDL_Rect * init_tabSDL_Rect(int taille){
    SDL_Rect* tab = (SDL_Rect*)malloc(taille * sizeof(SDL_Rect));  // Allocation du tableau
    for (int i=0; i<taille ; i++) {
        tab[i].x=0;             // Mise à zéro de tous ces composants
        tab[i].y=0;
        tab[i].w=0;
        tab[i].h=0;
    }
    return tab;
}

SDL_bool * init_tabSDL_bool(int taille){
    SDL_bool * tab = (SDL_bool*)malloc(taille * sizeof(SDL_bool));  // Allocation du tableau
    for (int i=0; i<taille ; i++) {
        tab[i]=SDL_FALSE;                   // Mise à FAUX
    }
    return tab;
}

SDL_bool * reset_tabSDL_bool(SDL_bool * tab, int taille){
    for (int i=0; i<taille ; i++) {
        tab[i]=SDL_FALSE;                   // Mise à FAUX
    }
    return tab;
}

SDL_bool * update_tabSourisSurBouton(SDL_bool * tabEtat,SDL_bool * tabSourisSurBouton,SDL_Rect * tabBoutonRect,int mouseX,int mouseY){
    if (((tabEtat[2])||(tabEtat[1]))&(!tabEtat[0])) {           // Si on est dans le jeu ou les crédits
        if ((mouseX>=tabBoutonRect[0].x) &                      // Si la souris se trouve sur le bouton de retour menu
            (mouseX<=tabBoutonRect[0].x+tabBoutonRect[0].w) &
            (mouseY>=tabBoutonRect[0].y) &
            (mouseY<=tabBoutonRect[0].y+tabBoutonRect[0].h)){       
            tabSourisSurBouton[0]=SDL_TRUE;                     // Alors on passe le booléen en question à vrai        
        }
    }
    else if((tabEtat[0])&(!tabEtat[1])&(!tabEtat[2])) {         // Si on est dans le menu
        for (int i=1;i<4;i++) {                                 // Pour tous les boutons du menu
            if ((mouseX>=tabBoutonRect[i].x) &                      // Si la souris se trouve sur ce bouton
                (mouseX<=tabBoutonRect[i].x+tabBoutonRect[i].w) &
                (mouseY>=tabBoutonRect[i].y) &
                (mouseY<=tabBoutonRect[i].y+tabBoutonRect[i].h)){       
                tabSourisSurBouton[i]=SDL_TRUE;                     // Alors on passe le booléen en question à vrai        
            }
        }
    }
    return tabSourisSurBouton;
}