#include "jeu.h"

void changerPositionJoueur(int **layerFixe, int **layerDynamique, position *j, int jNbr, int dir)
{
    if (dir == HAUT)
    {
        if (layerFixe[(*j).y - 1][(*j).x] == 0 || layerFixe[(*j).y - 1][(*j).x] == 2) // S'il peut aller sur la case
        {
            layerDynamique[(*j).y][(*j).x] = layerFixe[(*j).y][(*j).x]; // On place la bonne case où le joueur était
            (*j).y = (*j).y - 1;
            layerDynamique[(*j).y][(*j).x] = jNbr; // On place le joueur dans la matrice dynamique
        }
    }
    else if (dir == BAS)
    {
        if (layerFixe[(*j).y + 1][(*j).x] == 0 || layerFixe[(*j).y + 1][(*j).x] == 2) // S'il peut aller sur la case
        {
            layerDynamique[(*j).y][(*j).x] = layerFixe[(*j).y][(*j).x]; // On place la bonne case où le joueur était
            (*j).y = (*j).y + 1;
            layerDynamique[(*j).y][(*j).x] = jNbr; // On place le joueur dans la matrice dynamique
        }
    }
    else if (dir == GAUCHE)
    {
        if (layerFixe[(*j).y][(*j).x - 1] == 0 || layerFixe[(*j).y][(*j).x - 1] == 2) // S'il peut aller sur la case
        {
            layerDynamique[(*j).y][(*j).x] = layerFixe[(*j).y][(*j).x]; // On place la bonne case où le joueur était
            (*j).x = (*j).x - 1;
            layerDynamique[(*j).y][(*j).x] = jNbr; // On place le joueur dans la matrice dynamique
        }
    }
    else if (dir == DROITE)
    {
        if (layerFixe[(*j).y][(*j).x + 1] == 0 || layerFixe[(*j).y][(*j).x + 1] == 2) // S'il peut aller sur la case
        {
            layerDynamique[(*j).y][(*j).x] = layerFixe[(*j).y][(*j).x]; // On place la bonne case où le joueur était
            (*j).x = (*j).x + 1;
            layerDynamique[(*j).y][(*j).x] = jNbr; // On place le joueur dans la matrice dynamique
        }
    }
}
void verifTresors(int **layerFixe, position j, int *pointsJoueur, int * cp_tresor)
{
    if (layerFixe[j.y][j.x] == 2)
    {
        *pointsJoueur += 5;
        layerFixe[j.y][j.x] = 0;
        *cp_tresor -= 1;
    }
}
void verifJoueurs(int **layerFixe, int **layerDynamique, position *poule, position *renard, position *vipere, int *pointsPoule, int *pointsRenard, int *pointsVipere)
{
    int place = 0;
    int randx;
    int randy;
    // Poule mange vipere
    if ((abs((*poule).x - (*vipere).x) <= 1 && (*poule).y == (*vipere).y) || (abs((*poule).y - (*vipere).y) <= 1 && (*poule).x == (*vipere).x))
    {
        while (place == 0)
        {
            randx = rand() % 30;
            randy = rand() % 30;
            while (layerFixe[randy][randx] != 0)
            {
                randx = rand() % 30;
                randy = rand() % 30;
            }
            layerDynamique[(*vipere).y][(*vipere).x] = 0;
            (*vipere).x = randx;
            (*vipere).y = randy;
            layerDynamique[randy][randx] = 91;
            //*pointsPoule += 1;
            *pointsVipere -= 1;
            place = 1;
        }
    }

    place = 0;
    // Vipere mange renard
    if ((abs((*vipere).x - (*renard).x) <= 1 && (*vipere).y == (*renard).y) || (abs((*vipere).y - (*renard).y) <= 1 && (*vipere).x == (*renard).x))
    {

        while (place == 0)
        {
            randx = rand() % 30;
            randy = rand() % 30;
            while (layerFixe[randy][randx] != 0)
            {
                randx = rand() % 30;
                randy = rand() % 30;
            }
            layerDynamique[(*renard).y][(*renard).x] = 0;
            (*renard).x = randx;
            (*renard).y = randy;
            layerDynamique[randy][randx] = 81;
            //*pointsVipere += 1;
            *pointsRenard -= 1;
            place = 1;
        }
    }

    // Renard mange poule
    if ((abs((*poule).x - (*renard).x) <= 1 && (*poule).y == (*renard).y) || (abs((*poule).y - (*renard).y) <= 1 && (*poule).x == (*renard).x))
    {

        while (place == 0)
        {
            randx = rand() % 30;
            randy = rand() % 30;
            while (layerFixe[randy][randx] != 0)
            {
                randx = rand() % 30;
                randy = rand() % 30;
            }
            layerDynamique[(*poule).y][(*poule).x] = 0;
            (*poule).x = randx;
            (*poule).y = randy;
            layerDynamique[randy][randx] = 71;
            //*pointsRenard += 1;
            *pointsPoule -= 1;
            place = 1;
        }
    }
}

// j1 veut attraper j2 qui veut attraper j3 qui veut attraper j1
void tour(int **layerFixe, int **layerDynamique, position *j1, position *j2, position *j3, Regle *r1, Regle *r2, Regle *r3, int *pointsPoule, int *pointsRenard, int *pointsVipere, int spawnPoule[2], int spawnRenard[2], int spawnVipere[2], int points[100][2], int * cp_tresor)
{   
    int nbRegles;
    Regle regleChoisie; // Regle qui va s'appliquer

    // Actions du premier joueur
    Regle *r1Filtre = FiltrageRegles(r1, GetCasesAutour(layerDynamique, *j1), GetDirection(*j1, *j2), GetDirection(*j1, *j3), &nbRegles);
    if (nbRegles > 0)
    {
        regleChoisie = choix_regle(r1Filtre, nbRegles);
        switch (regleChoisie.action)
        {
        case HAUT:
            changerPositionJoueur(layerFixe, layerDynamique, j1, 71, HAUT);
            break;
        case BAS:
            changerPositionJoueur(layerFixe, layerDynamique, j1, 71, BAS);
            break;
        case GAUCHE:
            changerPositionJoueur(layerFixe, layerDynamique, j1, 71, GAUCHE);
            break;
        case DROITE:
            changerPositionJoueur(layerFixe, layerDynamique, j1, 71, DROITE);
        }
    }
    verifTresors(layerFixe, *j1, pointsPoule, cp_tresor);

    // Actions du deuxième joueur
    nbRegles = 0;
    Regle *r2Filtre = FiltrageRegles(r2, GetCasesAutour(layerDynamique, *j2), GetDirection(*j2, *j1), GetDirection(*j2, *j3), &nbRegles);
    if (nbRegles > 0)
    {
        regleChoisie = choix_regle(r2Filtre, nbRegles);
        switch (regleChoisie.action)
        {
        case HAUT:
            changerPositionJoueur(layerFixe, layerDynamique, j2, 81, HAUT);
            break;
        case BAS:
            changerPositionJoueur(layerFixe, layerDynamique, j2, 81, BAS);
            break;
        case GAUCHE:
            changerPositionJoueur(layerFixe, layerDynamique, j2, 81, GAUCHE);
            break;
        case DROITE:
            changerPositionJoueur(layerFixe, layerDynamique, j2, 81, DROITE);
        }
    }
    verifTresors(layerFixe, *j2, pointsRenard, cp_tresor);

    // Actions du troisième joueur
    nbRegles = 0;
    Regle *r3Filtre = FiltrageRegles(r3, GetCasesAutour(layerDynamique, *j3), GetDirection(*j3, *j1), GetDirection(*j3, *j2), &nbRegles);
    if (nbRegles > 0)
    {
        regleChoisie = choix_regle(r3Filtre, nbRegles);
        switch (regleChoisie.action)
        {
        case HAUT:
            changerPositionJoueur(layerFixe, layerDynamique, j3, 91, HAUT);
            break;
        case BAS:
            changerPositionJoueur(layerFixe, layerDynamique, j3, 91, BAS);
            break;
        case GAUCHE:
            changerPositionJoueur(layerFixe, layerDynamique, j3, 91, GAUCHE);
            break;
        case DROITE:
            changerPositionJoueur(layerFixe, layerDynamique, j3, 91, DROITE);
        }
    }
    verifTresors(layerFixe, *j3, pointsVipere, cp_tresor);

    verifJoueurs(layerFixe, layerDynamique, j1, j2, j3, pointsPoule, pointsRenard, pointsVipere);

    free(r1Filtre);
    free(r2Filtre);
    free(r3Filtre);
    // Regle * r1Filtre = FiltrageRegles(r1, GetCasesAutour(layerDynamique, j1), GetDirection(j1, j2), GetProximite(j1 ,j2),GetDirection(j1, j3), GetProximite(j1, j3), GetPointLePlusProche(points, j1, pointsRestants));
}