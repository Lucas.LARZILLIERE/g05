#include "Parralle.h"

int treatment(void *parameters)
{
    int *p = (int *)parameters; // p is now the address of an int, and not the address of a "???"
    printf("Compute with value: %d.\n", *p);
    return 0;
}

int treatment2(void *parameters)
{
    float p = *((float *)parameters); // the value starting at the address 'parameters' is interpreted as a float
    p = p * p - 1;
    // ...
    printf("I have computed with this value: %f.\n", p);
    return 0;
}

int trhead()
{
    thrd_t thread_handle_a;
    int a = 0;
    thrd_create(&thread_handle_a, treatment, &a);

    thrd_t thread_handle_b;
    float b = 2;
    thrd_create(&thread_handle_b, treatment2, &b);

    int error_code_of_thread_a = 0;
    int error_code_of_thread_b = 0;
    thrd_join(thread_handle_a, &error_code_of_thread_a);
    thrd_join(thread_handle_b, &error_code_of_thread_b);
    return 0;
}

void * partie(void * arg)
{
    argumentThread * arguments = (argumentThread* ) arg;

    // Code
    // On réinitialise les points
    int pointsPoule = 0;
    int pointsRenard = 0;
    int pointsVipere = 0;

    // On regénère la map dynamique
    arguments->layerFixe = initMatrice(30);
    arguments->layerDynamique = initMatrice(30);
    arguments->pointsRestants = 30;
    arguments->layerFixe = creation_du_monde(arguments->layerFixe, arguments->pointsRestants, 30, arguments->points, arguments->spawnPoule, arguments->spawnRenard, arguments->spawnVipere);
    placement_joueur(arguments->layerDynamique, arguments->layerFixe, 30, arguments->posPoule, arguments->posRenard, arguments->posVipere);
    (*j1).x = arguments->posPoule[0];
    (*j1).y = arguments->posPoule[1];
    (*j2).x = arguments->posRenard[0];
    (*j2).y = arguments->posRenard[1];
    (*j3).x = arguments->posVipere[0];
    (*j3).y = arguments->posVipere[1];

    for (int j = 0; j < 100; j++) // Chaque partie fait 100 tours
    {
        tour(arguments->layerFixe, arguments->layerDynamique, j1, j2, j3, r1, r2, r3, &pointsPoule, &pointsRenard, &pointsVipere, spawnPoule, spawnRenard, spawnVipere);
    }

    return NULL;
}