#include <SDL2/SDL.h>

// Permet d'initialiser une fenêtre
SDL_Window * initWindow(int x, int y, int w, int h, char * name);

// Permet d'initialiser un Renderer
SDL_Renderer * initRenderer(SDL_Window * window);

// Arrêter tout le rendu en SDL, en vérifiant ce qui ne va pas
void end_sdl(char ok,  char const *msg, SDL_Window *window, SDL_Renderer *renderer);