#include "textures_SDL.h"

SDL_Texture *load_texture_from_image(char *file_image_name, SDL_Window *window, SDL_Renderer *renderer)
{
    SDL_Surface *my_image = NULL;   // Variable de passage
    SDL_Texture *my_texture = NULL; // La texture

    my_image = IMG_Load(file_image_name); // Chargement de l'image dans la surface
                                          // image=SDL_LoadBMP(file_image_name); fonction standard de la SDL,
                                          // uniquement possible si l'image est au format bmp */
    if (my_image == NULL)
        end_sdl(0, "Chargement de l'image impossible", window, renderer);

    my_texture = SDL_CreateTextureFromSurface(renderer, my_image); // Chargement de l'image de la surface vers la texture
    SDL_FreeSurface(my_image);                                     // la SDL_Surface ne sert que comme élément transitoire
    if (my_texture == NULL)
        end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer);

    return my_texture;
}

void animateSprite(SDL_Texture *my_texture, SDL_Window *window, SDL_Renderer *renderer)
{
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);
    
    ///////////////////////
    // Init personnage ////
    ///////////////////////
    SDL_Rect
        source = {0},            // Rectangle définissant la zone totale de la planche
        window_dimensions = {0}, // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
        destination = {0},       // Rectangle définissant où la zone_source doit être déposée dans le renderer
        state = {0};             // Rectangle de la vignette en cours dans la planche
    SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h);    
    printf("debug\n");
    int nb_images = 2;                   // Seulement 2 vignettes
    float zoom = 4;                      
    int offset_x = source.w / nb_images, 
        offset_y = source.h;             
    state.x = 0;
    state.y = 1;
    state.w = offset_x;
    state.h = offset_y;
    printf("debug\n");
    
     SDL_bool
        program_on = SDL_TRUE,   // Booléen pour dire que le programme doit continuer
        event_utile = SDL_FALSE; // Booléen pour savoir si on a trouvé un event traité
    SDL_Event event;             // Evènement à traiter

    while (program_on) // La boucle des évènements
    {
        event_utile = SDL_FALSE;
        while (!event_utile && SDL_PollEvent(&event))
        {
            switch (event.type){ // En fonction de la valeur du type de cet évènement                         
            case SDL_QUIT:              // Si X de la fenêtre : on quitte le programme
                program_on = SDL_FALSE; 
                event_utile = SDL_TRUE;
                break;
            case SDL_KEYDOWN:           // Si touche appuyée
            switch (event.key.keysym.sym) {
               
                case SDLK_ESCAPE:       // Si échap : quitte la fenêtre
                    SDL_Quit();
                    break;
                }
            }
        }
    }

    SDL_RenderClear(renderer); // Effacer la fenêtre avant de rendre la main
}