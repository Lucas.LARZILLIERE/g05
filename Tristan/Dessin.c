#include <stdio.h>
#include "Dessin.h"
#include "utilSDL.h"

void draw(SDL_Renderer *renderer)
{
    SDL_Rect rectangle;
    const int WINDOW_WIDTH = 600;
    const int WINDOW_HEIGHT = 400;

    const int FLAG_WIDTH = 800;
    const int FLAG_HEIGHT = 800;
    const int VERTICAL_STRIPE_WIDTH = FLAG_WIDTH / 3;
    SDL_SetRenderDrawColor(renderer,
                           255, 255, 255, // mode Red, Green, Blue (tous dans 0..255)
                           255);     // 0 = transparent ; 255 = opaque

    // Dessiner le rectangle bleu
    SDL_SetRenderDrawColor(renderer, 0, 85, 164, 255);
    SDL_Rect blueRect = { 0, 0, VERTICAL_STRIPE_WIDTH, FLAG_HEIGHT };
    SDL_RenderFillRect(renderer, &blueRect);

    // Dessiner le rectangle blanc
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_Rect whiteRect = { VERTICAL_STRIPE_WIDTH, 0, VERTICAL_STRIPE_WIDTH, FLAG_HEIGHT };
    SDL_RenderFillRect(renderer, &whiteRect);

    // Dessiner le rectangle rouge
    SDL_SetRenderDrawColor(renderer, 239, 65, 53, 255);
    SDL_Rect redRect = { VERTICAL_STRIPE_WIDTH * 2, 0, VERTICAL_STRIPE_WIDTH, FLAG_HEIGHT };
    SDL_RenderFillRect(renderer, &redRect);

 
}

int Dessin()
{
    SDL_Window* window = NULL;
       SDL_Renderer* renderer = NULL;

       SDL_DisplayMode screen;

       SDL_GetCurrentDisplayMode(0, &screen);

       /* Création de la fenêtre */
       window = SDL_CreateWindow("Drapeau Français",
                 SDL_WINDOWPOS_CENTERED,
                 SDL_WINDOWPOS_CENTERED,
                 800,
                 800,
                 SDL_WINDOW_OPENGL);
       if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

       /* Création du renderer */
       renderer = SDL_CreateRenderer(window, -1,
                     SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
       if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

       /*********************************************************************************************************************/
       /*                                     On dessine dans le renderer                                                   */
       /*********************************************************************************************************************/
       /*             Cette partie pourrait avantageusement être remplacée par la boucle évènementielle                     */ 
       draw(renderer);                                      // appel de la fonction qui crée l'image  
       SDL_RenderPresent(renderer);                         // affichage
       SDL_Delay(10000);                                     // Pause exprimée en ms
                                                       

       /* on referme proprement la SDL */
       end_sdl(1, "Normal ending", window, renderer);
       return EXIT_SUCCESS;
}