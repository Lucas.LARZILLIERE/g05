#include "utilSDL.h"

SDL_Window * initWindow(int x, int y, int w, int h, char * name) {

    // Création de fenêtre :
    SDL_Window * window = NULL;
    window = SDL_CreateWindow(
        name, // codage en utf8, donc accents possibles
        x, y,  // coin haut gauche en haut gauche de l'écran
        w, h,  // dimensions de la fenêtre
        SDL_WINDOW_RESIZABLE);

    // Si erreur dans la création de la fenêtre :
    if (window == NULL)
    {
        SDL_Log("Error : SDL window creation - %s\n",
                SDL_GetError()); // échec de la création de la fenêtre
        SDL_Quit();              // On referme la SDL
        exit(EXIT_FAILURE);
    }

    return window;
}

SDL_Renderer * initRenderer(SDL_Window * window)
{
    SDL_Renderer* renderer = NULL;

    /* Création du renderer */
    renderer = SDL_CreateRenderer(window, -1,
                                  SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL)
        end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

    return renderer;
}

void end_sdl(char ok,  char const *msg, SDL_Window *window, SDL_Renderer *renderer) { // renderer à fermer
    char msg_formated[255];
    int l;

    if (!ok)
    { // Affichage de ce qui ne va pas
        strncpy(msg_formated, msg, 250);
        l = strlen(msg_formated);
        strcpy(msg_formated + l, " : %s\n");

        SDL_Log(msg_formated, SDL_GetError());
    }

    if (renderer != NULL)
    {                                  // Destruction si nécessaire du renderer
        SDL_DestroyRenderer(renderer); // Attention : on suppose que les NULL sont maintenus !!
        renderer = NULL;
    }
    if (window != NULL)
    {                              // Destruction si nécessaire de la fenêtre
        SDL_DestroyWindow(window); // Attention : on suppose que les NULL sont maintenus !!
        window = NULL;
    }

    SDL_Quit();

    if (!ok)
    { // On quitte si cela ne va pas
        exit(EXIT_FAILURE);
    }
}