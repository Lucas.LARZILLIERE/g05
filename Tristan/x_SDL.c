#include <stdio.h>
#include "x_SDL.h"
#include <stdbool.h>

void moveWindow()
{
    const int WINDOW_WIDTH = 640;
    const int WINDOW_HEIGHT = 480;

  SDL_bool
        program_on = SDL_TRUE,   // Booléen pour dire que le programme doit continuer
        paused = SDL_FALSE,      // Booléen pour dire que le programme est en pause
        event_utile = SDL_FALSE; // Booléen pour savoir si on a trouvé un event traité
    SDL_Event event;             // Evènement à traiter
   
    int cur1X,cur2X;
    int cur1Y,cur2Y;
    // On récupère la taille de l'écran :
    SDL_DisplayMode displayMode;
    SDL_GetCurrentDisplayMode(0, &displayMode);
    SDL_Window* window1 = SDL_CreateWindow("Fenêtre 1", 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT,0);
    SDL_Window* window2 = SDL_CreateWindow("Fenêtre 2", displayMode.w -WINDOW_WIDTH, displayMode.h-WINDOW_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT,0);
    while (program_on) // La boucle des évènements
    {
        
        // On va stocker la position de la fenêtre à chaque tour de boucle pour la modifier plus tard
        
        
        bool inverser = true;
        SDL_bool quit = SDL_FALSE;
        while (!quit) {
        SDL_Event event;
        while (SDL_PollEvent(&event) && !quit ) {
            if (event.type == SDL_QUIT) {
                quit = SDL_TRUE;
                
            }
        
            switch (event.type) {
                case SDL_QUIT:              // Si X de la fenêtre : on quitte le programme
                    program_on = SDL_FALSE; 
                    
                    quit = SDL_TRUE;

                break;
                case SDL_KEYDOWN: 
                switch (event.key.keysym.sym) {
                    case SDLK_SPACE:
                        if (inverser )
                        {
                            SDL_SetWindowPosition(window1, displayMode.w -WINDOW_WIDTH+100 , 100);
                            SDL_SetWindowPosition(window2, 0, displayMode.h-WINDOW_HEIGHT);
                            inverser=false;
                        }
                        else
                        {
                            SDL_SetWindowPosition(window1, 100, 100);
                            SDL_SetWindowPosition(window2, displayMode.w -WINDOW_WIDTH, displayMode.h-WINDOW_HEIGHT);
                            inverser=true;
                        }
                        event_utile = SDL_TRUE;
                    break;
                    case SDLK_r:
                            printf("oui tu as bien appuyé\n");
                            SDL_GetWindowPosition(window1, &cur1X, &cur1Y);
                            SDL_GetWindowPosition(window2, &cur2X, &cur2Y);
                           
                            while ((cur1Y<displayMode.h) && (cur2Y<displayMode.h) )
                            {
                                printf("cur1Y  %d\n",cur1Y);
                                printf("cur2Y  %d\n",cur2Y);
                                cur1Y+=10;
                                cur2Y+=10;
                                SDL_SetWindowPosition(window1, cur1X, cur1Y);
                                SDL_SetWindowPosition(window2, cur2X, cur2Y);
                               // SDL_GetWindowPosition(window1, &cur1X, &cur1Y);
                               // SDL_GetWindowPosition(window2, &cur2X, &cur2Y);
                                wait(2000);
                                
                            }
                            event_utile = SDL_TRUE;
                            break;

                    }
                break;
                }
                
            }
        }
    }
 // Libération des ressources
    SDL_DestroyWindow(window1);
    SDL_DestroyWindow(window2);
    SDL_Quit();
}



    


